<?php
  include_once('conexion/cnx.php');  //Agregando la Conexión a Base de Datos
  include_once('sesion.php');        //Agregando la Verificación de Sesión
?>
<!DOCTYPE html>
<html lang="es">
<head>
<!--Metadatos y Datos-->
<title>KTS Corp - Cartas de Trabajo</title>
<meta charset="utf-8">
<meta name="KTS Corp" content="Extranet">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="image/x-icon" href="../img/icono.png">

<!--Local-->
<link rel="stylesheet" type="text/css" href="../css/estilos.css">
<link rel="stylesheet" type="text/css" href="../css/sidebar.css">

<!--Bootstrap-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

<!--Iconos-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

<!--Símbolo de Carga-->
<div id="contenedor_loader">
  <div class="loader" id="loader"></div>
</div>

<!--Menú-->
<?php
  if($_SESSION['opc_user'] == 'usuarios_admin'){
    include_once('menuadmin.php');
  }elseif($_SESSION['opc_user'] == 'usuarios'){
    include_once('menuuser.php');
  }
?>

<div id="content-wrapper">
  <div class="container-fluid">
    <h2 class="text-center text-white titulos box">Solicitud de Cartas de Trabajo</h2>

    <div class="div1">
      <div class="div2">
        <div class="div3"><br>

          <div class="row">
            <div class="col-lg-6"><br>
              <form action="carta_trabajo.php" method="post" name="cartas" id="cartas">
                
                <div class="form-row">
                  <div class="form-group col-md-12">
                    <!--Dirigida-->
                    <label for="dirgida">Dirigida a:</label>
                    <div class="input-group">
                      <div class="input-group-text"><span class="fa fa-building" style="color: #395784;" title="Ejemplo" data-toggle="popover" data-trigger="hover" data-content="Banco Provincial."></span></div>
                      <input type="text" id="dirigida" name="dirigida" class="form-control" placeholder="Dirigida a" maxlength="32">
                    </div>
                  </div>
                </div>

                <div class="form-row">
                  <div class="form-group col-md-12">
                    <!--Fecha-->
                    <label for="fecha">Fecha de solicitud:</label>
                    <div class="input-group">
                      <div class="input-group-text"><span class="fa fa-calendar" style="color: #395784;" title="Ejemplo" data-toggle="popover" data-trigger="hover" data-content="01/01/2018"></span></div>
                      <input type="date" id="fecha" name="fecha" class="form-control" placeholder="Fecha de Solicitud">
                    </div>
                  </div>
                </div>
                <br>
                <!--Enviar-->
                <input type="button" id="enviar" name="solicitar" value="Solicitar" class="btn btn-block">
              </form><br>
            </div>

            <div class="col-lg-6 text-center"><br>
              <i class="fa fa-file-text-o box" style="font-size: 200px; color: #395784;"></i>
              <br><br>
            </div>
          </div>
        </div>
      </div>
    </div>
    <br>
    <strong>La carta de solicitud de constancia de trabajo es un documento formal que debes presentar en el Departamento de Recursos Humanos de la empresa en la que has trabajado con la finalidad de solicitar un certificado laboral o constancia de trabajo.</strong>
    <hr>
    <p>Los datos registrados en el sistema se envían mediante correo electrónico para el Departamento de Recursos Humanos junto con los datos que se completan en el formulario para la solicitud de carta de trabajo.</p>
    <footer class="sticky-footer">
      <div class="container my-auto">
        <div class="copyright text-center my-auto">
          © <span id="output"></span> | KTS Corp - Todos los derechos reservados | RIF J-29454989-6
        </div>
      </div>
    </footer>
  </div>
</div>

<!--JS-->
<script src="../js/funciones.js"></script>
<script src="../js/carta.js"></script>
<script src="../js/sidebar.js"></script>

</body>
</html>