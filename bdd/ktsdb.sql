-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-10-2018 a las 17:47:11
-- Versión del servidor: 10.1.35-MariaDB
-- Versión de PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ktsdb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `chkcontacto`
--

CREATE TABLE `chkcontacto` (
  `idchkcontacto` int(11) NOT NULL,
  `idcontacto` int(11) DEFAULT NULL,
  `idchk` int(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `chkcontacto`
--

INSERT INTO `chkcontacto` (`idchkcontacto`, `idcontacto`, `idchk`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto`
--

CREATE TABLE `contacto` (
  `idcontacto` int(11) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `edad` int(2) NOT NULL,
  `correo` varchar(64) NOT NULL,
  `idcodigo` int(1) NOT NULL,
  `telefono` int(7) NOT NULL,
  `mensaje` varchar(500) NOT NULL,
  `idradio` int(11) NOT NULL,
  `idarchivo` int(11) DEFAULT NULL,
  `idopciones` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto_archivos`
--

CREATE TABLE `contacto_archivos` (
  `idarchivo` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `tipo` varchar(90) NOT NULL,
  `size` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto_checkboxs`
--

CREATE TABLE `contacto_checkboxs` (
  `idchk` int(8) NOT NULL,
  `descripchk` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contacto_checkboxs`
--

INSERT INTO `contacto_checkboxs` (`idchk`, `descripchk`) VALUES
(1, 'E-mail'),
(2, 'Telefono');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto_codigo`
--

CREATE TABLE `contacto_codigo` (
  `idcodigo` int(11) NOT NULL,
  `descripcodigo` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contacto_codigo`
--

INSERT INTO `contacto_codigo` (`idcodigo`, `descripcodigo`) VALUES
(1, 414),
(2, 424),
(3, 416),
(4, 426),
(5, 412);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto_opciones`
--

CREATE TABLE `contacto_opciones` (
  `idopciones` int(11) NOT NULL,
  `descripopc` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contacto_opciones`
--

INSERT INTO `contacto_opciones` (`idopciones`, `descripopc`) VALUES
(1, 'Finanzas y Contabilidad (KTS Corp)'),
(2, 'Control y Gestión (KTS Corp)'),
(3, 'Administración (KTS Corp - Max Ferrer - Anatel - Netdata)'),
(4, 'Procura y Logística (KTS Corp - Max Ferrer - Anatel)'),
(5, 'Talento Humano (KTS Corp)'),
(6, 'Soporte Técnico (KTS Corp)'),
(7, 'Operaciones e Implementación (Mindforce - Netdata)'),
(8, 'Ventas (Max Ferrer - Anatel - Netdata)');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto_radios`
--

CREATE TABLE `contacto_radios` (
  `idradio` int(8) NOT NULL,
  `descriprad` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contacto_radios`
--

INSERT INTO `contacto_radios` (`idradio`, `descriprad`) VALUES
(1, 'Empleo'),
(2, 'Pasantias');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idusuario` int(11) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `cedula` bigint(10) NOT NULL,
  `idcargo` int(11) NOT NULL,
  `idempresa` int(11) NOT NULL,
  `clave` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idusuario`, `nombre`, `apellido`, `cedula`, `idcargo`, `idempresa`, `clave`) VALUES
(1, 'David', 'Nery', 25345882, 3, 2, '12345'),
(2, 'Diego', 'Avila', 26524982, 4, 5, '12345678'),
(3, 'Daniel', 'Boscan', 25540296, 2, 4, '12345678');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_admin`
--

CREATE TABLE `usuarios_admin` (
  `idadmin` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(40) NOT NULL,
  `cedula` bigint(10) NOT NULL,
  `idcargo` int(11) NOT NULL,
  `idempresa` int(11) NOT NULL,
  `clave` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios_admin`
--

INSERT INTO `usuarios_admin` (`idadmin`, `nombre`, `apellido`, `cedula`, `idcargo`, `idempresa`, `clave`) VALUES
(1, 'David', 'Nery', 25345882, 4, 2, '12345678'),
(2, 'Diego', 'Avila', 26524982, 3, 2, '12345678'),
(3, 'Daniel', 'Boscan', 25540296, 5, 3, '12345678');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_cargo`
--

CREATE TABLE `usuarios_cargo` (
  `idcargo` int(11) NOT NULL,
  `descripcargo` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios_cargo`
--

INSERT INTO `usuarios_cargo` (`idcargo`, `descripcargo`) VALUES
(1, 'Administracion'),
(2, 'Finanzas y Contabilidad'),
(3, 'Talento Humano'),
(4, 'Operaciones e Implementación'),
(5, 'Ventas'),
(6, 'Control y Gestión'),
(7, 'Procura y Logística'),
(8, 'Soporte Técnico');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_empresa`
--

CREATE TABLE `usuarios_empresa` (
  `idempresa` int(11) NOT NULL,
  `descripempresa` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios_empresa`
--

INSERT INTO `usuarios_empresa` (`idempresa`, `descripempresa`) VALUES
(1, 'Anatel'),
(2, 'KTS Corp'),
(3, 'Max Ferrer'),
(4, 'Mindforce'),
(5, 'Netdata'),
(6, 'Neutron');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `chkcontacto`
--
ALTER TABLE `chkcontacto`
  ADD PRIMARY KEY (`idchkcontacto`);

--
-- Indices de la tabla `contacto`
--
ALTER TABLE `contacto`
  ADD PRIMARY KEY (`idcontacto`);

--
-- Indices de la tabla `contacto_archivos`
--
ALTER TABLE `contacto_archivos`
  ADD PRIMARY KEY (`idarchivo`);

--
-- Indices de la tabla `contacto_checkboxs`
--
ALTER TABLE `contacto_checkboxs`
  ADD PRIMARY KEY (`idchk`);

--
-- Indices de la tabla `contacto_codigo`
--
ALTER TABLE `contacto_codigo`
  ADD PRIMARY KEY (`idcodigo`);

--
-- Indices de la tabla `contacto_opciones`
--
ALTER TABLE `contacto_opciones`
  ADD PRIMARY KEY (`idopciones`);

--
-- Indices de la tabla `contacto_radios`
--
ALTER TABLE `contacto_radios`
  ADD PRIMARY KEY (`idradio`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idusuario`),
  ADD UNIQUE KEY `cedula` (`cedula`);

--
-- Indices de la tabla `usuarios_admin`
--
ALTER TABLE `usuarios_admin`
  ADD PRIMARY KEY (`idadmin`),
  ADD UNIQUE KEY `cedula` (`cedula`);

--
-- Indices de la tabla `usuarios_cargo`
--
ALTER TABLE `usuarios_cargo`
  ADD PRIMARY KEY (`idcargo`);

--
-- Indices de la tabla `usuarios_empresa`
--
ALTER TABLE `usuarios_empresa`
  ADD PRIMARY KEY (`idempresa`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `chkcontacto`
--
ALTER TABLE `chkcontacto`
  MODIFY `idchkcontacto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `contacto`
--
ALTER TABLE `contacto`
  MODIFY `idcontacto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `contacto_archivos`
--
ALTER TABLE `contacto_archivos`
  MODIFY `idarchivo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `contacto_checkboxs`
--
ALTER TABLE `contacto_checkboxs`
  MODIFY `idchk` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `contacto_codigo`
--
ALTER TABLE `contacto_codigo`
  MODIFY `idcodigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `contacto_opciones`
--
ALTER TABLE `contacto_opciones`
  MODIFY `idopciones` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `contacto_radios`
--
ALTER TABLE `contacto_radios`
  MODIFY `idradio` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idusuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `usuarios_admin`
--
ALTER TABLE `usuarios_admin`
  MODIFY `idadmin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `usuarios_cargo`
--
ALTER TABLE `usuarios_cargo`
  MODIFY `idcargo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `usuarios_empresa`
--
ALTER TABLE `usuarios_empresa`
  MODIFY `idempresa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
