<?php
    include_once("sesion.php");
    include_once("conexion/cnx.php");
    include_once('../fpdf/fpdf.php');

        class PDF extends FPDF {
            function Header() {
                $this->SetFont('Arial','B',15);
                
                $this->Cell(12);
            }
            function Footer() {

                $this->Cell(180,0,'','T',1,'',true);
                
                $this->SetY(-15);
                
                $this->SetFont('Arial','',8);
             
            }
        }
        
        $pdf = new PDF('P','mm','A4');

        $pdf->AliasNbPages('1');
        
        $pdf->SetAutoPageBreak(true,15);
        $pdf->AddPage();
        
        $pdf->SetFont('Arial','',9);
        $pdf->SetDrawColor(180,180,255);
        $cont = 1;
        
        $pdf = new PDF();
        $pdf->AddPage();

        $pdf->SetFont('Arial','B',10);
        $pdf->SetFillColor(255,255,255);
        $pdf->SetDrawColor(255,255,255);
        $pdf->Ln(50);
        $pdf->SetDrawColor(180,180,255);
        $pdf->Cell(187,12,'DEPARTAMENTO DE TALENTO HUMANO',1,1,'C', true);
        $pdf->Cell(93.5,10,'SOLICITUD DE PERMISOS',1,0,'C', true);
        $pdf->Cell(93.5,10,utf8_decode('CÓDIGO: TH-F03'),1,0,'C', true);
        $pdf->Ln(15);
        $pdf->Cell(187.5,5,utf8_decode('Maracaibo,____ de ______ del año ____.'),0,0,'R',true);
        $pdf->Ln(10);
        $pdf->Cell(20,5,utf8_decode('Señores:'),0,1,'',true);
        $pdf->Ln(4);
        $pdf->Cell(22,5,'KTS Corp.',0,0,'',true);
        $pdf->Cell(5,4,'',1,0,'',true);
        $pdf->Cell(22,5,'Max Ferrer',0,0,'C',true);
        $pdf->Cell(5,4,'',1,0,'',true);
        $pdf->Cell(16,5,'Anatel',0,0,'',true);
        $pdf->Cell(5,4,'',1,0,'',true);
        $pdf->Cell(22,5,'Mindforce',0,0,'',true);
        $pdf->Cell(5,4,'',1,0,'',true);
        $pdf->Cell(18,5,'Neutron',0,0,'',true);
        $pdf->Cell(5,4,'',1,0,'',true);
        $pdf->Cell(18,5,'Netdata',0,0,'',true);
        $pdf->Cell(5,4,'',1,0,'',true);
        $pdf->Ln(10);
        $pdf->Cell(184,5,'Por medio de la presente, Yo ___________________ titular de la C.I. No.________________, me dirijo a ustedes',0,0,'C',true);
        $pdf->Ln(5);
        $pdf->Cell(189,5,utf8_decode('para solicitar permiso para ausentarme de mis funciones laborales el día: ________________-________________,'),0,0,'C',true);
        $pdf->Ln(5);
        $pdf->Cell(126,5,utf8_decode('a los días: ________/_________/________ de los corrientes por motivo de:'),0,0,'C',true);
        $pdf->Ln(10);
        $pdf->Cell(82,5,utf8_decode('Consulta Médica ______'),0,0,'C',true);
        $pdf->Cell(80.5,5,'Cita en Colegio ______',0,0,'C',true);
        $pdf->Ln(10);
        $pdf->Cell(80,5,utf8_decode('Examen Médico ______'),0,0,'C',true);
        $pdf->Cell(80.5,5,'Familiar Enfermo ______',0,0,'C',true);
        $pdf->Ln(10);
        $pdf->Cell(96,5,'Estudios ______',0,0,'C',true);
        $pdf->Cell(70.5,5,'Otros ______',0,0,'C',true);
        $pdf->Ln(15);
        $pdf->Cell(187,5,'Sin otro particular, me suscribo de ustedes',0,0,'C',true);
        $pdf->Ln(10);
        $pdf->Cell(187,5,'Atentamente:',0,0,'C',true);
        $pdf->Ln(10);
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(67,20,'Nombre del Solicitante:',1,0,'L',true);
        $pdf->Cell(50,30,'',1,0,'C',true);
        $pdf->Cell(70,20,'Solicitud Autorizada por el Jefe Inmediato:',1,1,'L',true);
        $pdf->Ln(0);
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(67,20,'Firma del Solicitante:',1,0,'L',true);
        $pdf->Cell(50,20,'Huella Dactilar del Trabajador',1,0,'C',true);
        $pdf->Cell(70,20,'Recibido por: Dpto. Talento Humano:',1,1,'L',true);
        $pdf->Ln(2);
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(95,5,'Permiso Remunerado: _____',0,0,'C',true);
        $pdf->Cell(95,5,'Permiso No Remunerado: _____',0,1,'C',true);
        $pdf->Ln(5);
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(40,10,'OBSERVACIONES:',0,1,'C',true);
        $pdf->Cell(187,10,'________________________________________________________________________________________________________________',0,1,'C',true);
        $pdf->Cell(187,10,'________________________________________________________________________________________________________________',0,1,'C',true);
        
        ob_end_clean();
        $pdf->OutPut();
?>