$(function(){

	$('#fecha').on({
		focusin: function(){
			$(this).removeClass( 'border-success border-warning border-danger' );
		},
		focusout: function(){
			var elementoT = document.cartas.fecha;
			if( elementoT.value === '' )
			{
				$(this).addClass( 'border-warning' );
				$('#enviar').removeAttr( 'type','submit' );
				$('#enviar').attr( 'type','button' );
			}
			else
			{
				$(this).addClass( 'border-success' );
				$('#enviar').removeAttr( 'type','button' );
				$('#enviar').attr( 'type','submit' );
			}
		},
		keyup: function(){
		}
	});

	$('#dirigida').on({
		focusin: function(){
			$(this).removeClass( 'border-success border-warning border-danger' );
		},
		focusout: function(){
			var elemento = document.cartas.dirigida;
			var re = /^[A-Za-z-%.,0-9 ]*$/i;
			if( elemento.value === '' )
			{
				$(this).addClass( 'border-warning' );
				$('#enviar').removeAttr( 'type','submit' );
				$('#enviar').attr( 'type','button' );
			}
			else
			{
				if ( !re.test(cartas.dirigida.value) )
				{
					$(this).addClass( 'border-danger' );
					$('#enviar').removeAttr( 'type','submit' );
					$('#enviar').attr( 'type','button' );
				}
				else
				{
					$(this).addClass( 'border-success' );
					$('#enviar').removeAttr( 'type','button' );
					$('#enviar').attr( 'type','submit' );
				}
			}
		},
		keyup: function(){
		}
	});

	$('#enviar').on({
		click: function(){
			
			if ( document.cartas.fecha.value === '' )
			{
				$('#fecha').addClass( 'border-warning' );
				$('#enviar').removeAttr( 'type','submit' );
				$('#enviar').attr( 'type','button' );
			}
			if ( document.cartas.dirigida.value === '' )
			{
				$('#dirigida').addClass( 'border-warning' );
				$('#enviar').removeAttr( 'type','submit' );
				$('#enviar').attr( 'type','button' );
			}
		}
	});
});