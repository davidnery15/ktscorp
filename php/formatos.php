<?php
  include_once('conexion/cnx.php');  //Agregando la Conexión a Base de Datos
  include_once('sesion.php');        //Agregando la Verificación de Sesión
?>
<!DOCTYPE html>
<html lang="es">
<head>
<!--Metadatos y Datos-->
<title>KTS Corp - Formatos</title>
<meta charset="utf-8">
<meta name="KTS Corp" content="Extranet">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="image/x-icon" href="../img/icono.png">

<!--Local-->
<link rel="stylesheet" type="text/css" href="../css/estilos.css">
<link rel="stylesheet" type="text/css" href="../css/sidebar.css">

<!--Bootstrap-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

<!--Iconos-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

<!--Símbolo de Carga-->
<div id="contenedor_loader">
  <div class="loader" id="loader"></div>
</div>

<!--Menú-->
<?php
  if($_SESSION['opc_user'] == 'usuarios_admin'){
    include_once('menuadmin.php');
  }elseif($_SESSION['opc_user'] == 'usuarios'){
    include_once('menuuser.php');
  }
?>

<div id="content-wrapper">
  <div class="container-fluid text-center">
    <h2 class="text-center text-white titulos box">Formatos</h2>

    <div class="div1">
      <div class="div2">
        <div class="div3"><br>
          
          <div class="row">
            <div class="col-lg-6"><br>
              <center><i class="fa fa-address-book-o box" style="font-size: 100px; color: #395784;"></i></center>
              <br>
              <h3 class="text-center">Directorio de Contactos</h3>
              <hr style="background-color: #848584">
              <p>En este formato se encuentran <strong>todos los contactos</strong> de KTS Corp., Max Ferrer, Anatel, Netdata, Mindforce, contactos externos y contactos de oficina, aquí se presentan sus datos básicos de contacto ya sean sus nombres, números de teléfono y dirección de correo electrónico en caso de necesitar comunicarse con alguno de ellos.</p>
              <a href="archivos/directorio_contactos.pdf" target="_blank"><button type="button" class="btn" style="font-size: 18px;">Generar PDF</button></a>
            </div>
            <div class="col-lg-6"><br>
              <center><i class="fa fa-ship box" style="font-size: 100px; color: #395784;"></i></center>
              <br>
              <h3 class="text-center">Solicitud de Vacaciones</h3>
              <hr style="background-color: #848584">
              <p>En este formato se encuentra una solicitud de vacaciones en el cual el usuario debe imprimir y completar el formato con sus datos básicos, <strong>nombre, apellido, cedula y a quien va dirigida. </strong>Presentar dicho formato en el área de talento humano para su revisión y aprobación.</p>
              <a href="solicitud_vacaciones.php" target="_blank"><button type="button" class="btn" style="font-size: 18px;">Generar PDF</button></a>    
            </div>
          </div>

          <br>

          <div class="row slideanim">
            <div class="col-sm-4"><br>
              <center><i class="fa fa-signing box" style="font-size: 100px; color: #395784;"></i></center>
              <br>
              <h3 class="text-center">Solicitud de Permisos</h3>
              <hr style="background-color: #848584">
              <p>En este formato se encuentra una solicitud de permisos en el cual el usuario debe imprimir y completar el formato con sus datos básicos, <strong>nombre, apellido, cedula, a quien va dirigida y establecer el o los motivos por el cual se solicitara el permiso del usuario. </strong>Presentar dicho formato en el área de talento humano para su revisión y aprobación.</p>
              <a href="solicitud_permisos.php" target="_blank"><button type="button" class="btn" style="font-size: 18px;">Generar PDF</button></a>
            </div>
            <div class="col-sm-4"><br>
              <center><i class="fa fa-id-badge box" style="font-size: 100px; color: #395784;"></i></center>
              <br>
              <h3 class="text-center">Requisición de Personal</h3>
              <hr style="background-color: #848584">
              <p>En este formato se encuentra una solicitud de requisición de personal en el cual el usuario debe imprimir y completar el formato con sus datos básicos, <strong>nombre, apellido, cedula, a quien va dirigida, fecha, cargo requerido, Nro. de vacante, fecha de inicio, modalidad y motivo. </strong>Presentar dicho formato en el área de talento humano para su revisión y aprobación.</p>
              <a href="requisicion_personal.php" target="_blank"><button type="button" class="btn" style="font-size: 18px;">Generar PDF</button></a>
            </div>
            <div class="col-sm-4"><br>
              <center><i class="fa fa-money box" style="font-size: 100px; color: #395784;"></i></center>
              <br>
              <h3 class="text-center">Anticipo de Prestaciones</h3>
              <hr style="background-color: #848584">
              <p>En este formato se encuentra una solicitud de anticipo de prestaciones en el cual el usuario debe imprimir y completar el formato con sus datos básicos, <strong>nombre, apellido, cedula, a quien va dirigida, establecer el monto solicitado y el motivo del mismo. </strong>presentar dicho formato en el área de talento humano para su revisión y aprobación.</p>
              <a href="anticipo_prestaciones.php" target="_blank"><button type="button" class="btn" style="font-size: 18px;">Generar PDF</button></a>
            </div>
          </div>
          <br>
        </div>
      </div>
    </div>
    <footer class="sticky-footer">
      <div class="container my-auto">
        <div class="copyright text-center my-auto">
          © <span id="output"></span> | KTS Corp - Todos los derechos reservados | RIF J-29454989-6
        </div>
      </div>
    </footer>
  </div>
</div>

<!--JS-->
<script src="../js/funciones.js"></script>
<script src="../js/sidebar.js"></script>

</body>
</html>