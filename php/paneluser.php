<?php
  include_once('conexion/cnx.php');  //Agregando la Conexión a Base de Datos
  include_once('sesion.php');        //Agregando la Verificación de Sesión
?>
<!DOCTYPE html>
<html lang="es">
<head>
<!--Metadatos y Datos-->
<title>KTS Corp - Empleado</title>
<meta charset="utf-8">
<meta name="KTS Corp" content="Extranet">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="image/x-icon" href="../img/icono.png">

<!--Local-->
<link rel="stylesheet" type="text/css" href="../css/estilos.css">
<link rel="stylesheet" type="text/css" href="../css/sidebar.css">

<!--Bootstrap-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

<!--Iconos-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

<!--Símbolo de Carga-->
<div id="contenedor_loader">
  <div class="loader" id="loader"></div>
</div>

<!--Menú-->
<?php
  include_once('menuuser.php');
?>

<div id="content-wrapper">
  <div class="container-fluid text-center">
    <h2 class="text-center text-white titulos box">Empleado</h2>

    <div class="div1">
      <div class="div2">
        <div class="div3"><br>
          
          <div class="row">
            <div class="col-sm-4">
              <i class="fa fa-file-text-o box" style="font-size: 100px; color: #395784;"></i>
              <br>
              <h3 class="text-center">Cartas de Trabajo</h3>
              <hr style="background-color: #848584">
              <p>Cualquier usuario podra solicitar cartas de trabajo mediante un formulario del mismo, el cuál <strong>debe ser entregado y procesado por el personal de talento humano de la empresa.</strong></p>
            </div>
            <div class="col-sm-4">
              <i class="fa fa-file-pdf-o box" style="font-size: 100px; color: #395784;"></i>
              <br>
              <h3 class="text-center">Formatos</h3>
              <hr style="background-color: #848584">
              <p>Esta sección contiene todos lo formatos de la empresa, los cuales son: <strong>Directorio de Contactos, solicitud de vacaciones, permisos, requisición de personal y solicitud de anticipos de prestaciones sociales.</strong></p>       
            </div>
            <div class="col-sm-4">
              <i class="fa fa-book box" style="font-size: 100px; color: #395784;"></i>
              <br>
              <h3 class="text-center">Políticas</h3>
              <hr style="background-color: #848584">
              <p>Esta sección contiene las <strong>políticas de recomendación, políticas de vacaciones, políticas de prestaciones sociales y las políticas del bono de transporte</strong> dirigida a todo el personal de KTS Corp.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <footer class="sticky-footer">
      <div class="container my-auto">
        <div class="copyright text-center my-auto">
          © <span id="output"></span> | KTS Corp - Todos los derechos reservados | RIF J-29454989-6
        </div>
      </div>
    </footer>
  </div>
</div>

<!--JS-->
<script src="../js/funciones.js"></script>
<script src="../js/sidebar.js"></script>

</body>
</html>