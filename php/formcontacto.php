<?php
    //Validando que exista el boton de envío
    if ( isset($_POST['enviar']) ){
        //Incluyendo la conexión a la Base De Datos
        include_once('conexion/cnx.php');
        //Captando el valor de los campos en el formulario
        $nombre      = $_POST['nombre'];
        $apellido    = $_POST['apellido'];
        $opc_edad    = $_POST['opc_edad'];
        $correo      = $_POST['email'];
        $num_codigo  = $_POST['num_codigo'];
        $numero      = $_POST['numero'];
        $txtmensaje  = $_POST['txtmensaje'];
        $radio       = $_POST['radio'];
        $opc_empresa = $_POST['opc_empresa'];
        $archivo     = $_FILES['archivo'];

        //Validando que exista algún valor en los campos
        if (empty($nombre) || empty($apellido) || empty($opc_edad) || empty($correo) || empty($num_codigo) || empty($numero) || empty($txtmensaje) || empty($radio) || empty($opc_empresa) )
        {
            echo '<script> alert("Algunos campos se encuentran vacíos"); location.href="/"; </script>';
        }else{

            //Comprobando si existe archivo y almacenandolo
            if( $archivo['size'] > 0 ){
                $ar_nombre = $_FILES['archivo']['name'];
                $ar_tipo   = $_FILES['archivo']['type'];
                $ar_tamaño = $_FILES['archivo']['size'];

                $queryInsertarF = "INSERT INTO contacto_archivos ( idarchivo, nombre, tipo, size ) values ( null, '$ar_nombre', '$ar_tipo', '$ar_tamaño' )";
                $QI1 = mysqli_query($cnx, $queryInsertarF) or die(mysqli_error($cnx));

                $querybuscaridF = "SELECT idarchivo FROM contacto_archivos where nombre = '$ar_nombre' ";
                $QBF = mysqli_query($cnx, $querybuscaridF) or die(mysqli_error($cnx));
                while ( $fila=mysqli_fetch_array($QBF) )
		        {
		    	    $idarchivo = $fila['idarchivo'];
		        }

                move_uploaded_file($_FILES['archivo']['tmp_name'], '../cv/'.$_FILES['archivo']['name']);
            }else{
                $idarchivo = 0;
            }

            //Insertando datos en la tabla de Contacto
            $queryInsertarD = "INSERT INTO contacto ( idcontacto, nombre, apellido, edad, correo, idcodigo, telefono, mensaje, idradio, idarchivo, idopciones ) values ( null, '$nombre', '$apellido', '$opc_edad', '$correo', '$num_codigo', '$numero', '$txtmensaje', '$radio', '$idarchivo', '$opc_empresa' )";
            $QI1 = mysqli_query($cnx, $queryInsertarD) or die(mysqli_error($cnx));

            // Buscando en la Base de Datos el "idcontacto"
		    $querybuscarC = "SELECT idcontacto FROM contacto where correo='$correo' ";
            $QB = mysqli_query($cnx, $querybuscarC) or die(mysqli_error($cnx));
            while ( $fila=mysqli_fetch_array($QB) )
		    {
		    	$idcontacto = $fila['idcontacto'];
		    }

            //Insertando datos en la tabla de Chkcontacto
            if (isset($_REQUEST['checkbox'])){
		        foreach ($_REQUEST['checkbox'] as $idchk)
		        {
		    	    $queryInsertarChk = "INSERT INTO chkcontacto( idchkcontacto, idcontacto, idchk ) values ( null, '$idcontacto', '$idchk' )";			
                    $QI2 = mysqli_query($cnx, $queryInsertarChk) or die(mysqli_error($cnx));
                }
            }
        }

        //Captando valores para el correo
        $querybuscarN = "SELECT * FROM contacto_codigo where idcodigo='$num_codigo' ";
        $QB = mysqli_query($cnx, $querybuscarN) or die(mysqli_error($cnx));
        while ( $fila=mysqli_fetch_array($QB) )
		{
		    $cod = $fila['descripcodigo'];
        }

        $querybuscarE = "SELECT * FROM contacto_opciones where idopciones='$opc_empresa' ";
        $QB = mysqli_query($cnx, $querybuscarE) or die(mysqli_error($cnx));
        while ( $fila=mysqli_fetch_array($QB) )
		{
		    $com = $fila['descripopc'];
        }

        $querybuscarS = "SELECT * FROM contacto_radios where idradio='$radio' ";
        $QB = mysqli_query($cnx, $querybuscarS) or die(mysqli_error($cnx));
        while ( $fila=mysqli_fetch_array($QB) )
		{
		    $sol = $fila['descriprad'];
		}

        // Preparación del correo
        
        $to = 'davidnery15@gmail.com';
        $subject = 'Contacto KTS Corp';
        $fromName = $nombre." ".$apellido;
        $headers = "From: $fromName"." <".$correo.">";

        $message  = "Nombre: ".$nombre." ".$apellido;
        $message .= "\nEdad: ".$opc_edad;
        $message .= "\nCorreo: ".$correo;
        $message .= "\nTelefono: 0".$cod."-".$numero;
        $message .= "\nPuesto de Trabajo: ".$com;
        $message .= "\nSolicita: ".$sol;
        $message .= "\nMensaje: ".$txtmensaje;

        if(isset($_REQUEST['checkbox']))
        {
            $mt = 0;
            foreach ($_REQUEST['checkbox'] as $idchk)
            {
                if($mt == 0){
                    $mt = 1;
                    $message .= "\nComunicarse Por: ";
                }

                $qbch = "SELECT * FROM contacto_checkboxs where idchk='$idchk' ";
                $QBC = mysqli_query($cnx, $qbch) or die(mysqli_error($cnx));
                while ( $fila=mysqli_fetch_array($QBC) )
		        {
		            $message .= "-".$fila['descripchk']." ";
		        }
            }
        }

        $mail = @mail($to, $subject, $message, $headers);
        
        // Confirmando envío del correo
        if ($mail){
            echo "<script>alert('Datos enviados, le responderemos lo más pronto posible.');location.href ='javascript:history.back()';</script>";
        }else{
            echo "<script>alert('Error al enviar los datos');location.href ='javascript:history.back()';</script>";
        }
    }

?>