<?php
  include_once('php/conexion/cnx.php');  //Agregando la Conexión a Base de Datos
?>
<!DOCTYPE html>
<html lang="es">
<head>
<!--Metadatos y Datos-->
<title>KTS Corp</title>
<meta charset="utf-8">
<meta name="KTS Corp" content="Extranet">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="image/x-icon" href="img/icono.png">

<!--Local-->
<link rel="stylesheet" type="text/css" href="css/estilos.css">
<link rel="stylesheet" type="text/css" href="css/parallax.css">

<!--Bootstrap-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

<!--Iconos-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body id="inicio">

<!--Símbolo de Carga-->
<div id="contenedor_loader">
  <div class="loader" id="loader"></div>
</div>

<!--Back to Top-->
<div class="to-top box">
  <a class="back-to-top fa fa-chevron-up" href="#"></a>
</div>

<!--Encabezado-->
<div style="background: #848584;"><br><br>
	<header class="jumbotron text-center" style="margin-bottom:0; background-color:#848584;">
    <img src="img/kts.png" class="img-fluid inicio"> 
  </header>
</div>

<!--Menú-->
<nav class="navbar navbar-expand-md navbar-dark fixed-top">

  <!--Inicio-->
  <a class="navbar-brand" href="#inicio"><button class="btn" style="font-size: 22px">KTS Corp</button></a>

  <!--Collapse-->
  <button class="navbar-toggler" class="btn" type="button" data-toggle="collapse" data-target="#collapsibleNavbar"><span class="navbar-toggler-icon"></span></button>

  <!--Links-->
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
  	<div class="navbar-nav">
  		<a class="nav-item nav-link" href="#empresa"><button class="btn" style="font-size: 17px;">Empresa</button></a>
      <a class="nav-item nav-link" href="#servicios"><button class="btn" style="font-size: 17px;">Servicios</button></a>
      <a class="nav-item nav-link" href="#clientes"><button class="btn" style="font-size: 17px;">Clientes</button></a>

      <!--Contáctanos-->
      <ul class="navbar-nav">
  	    <li class="nav-item dropdown">
  		    <span class="nav-link" href="#"><button class="btn dropdown-toggle" style="font-size: 17px;" data-toggle="dropdown">Contáctanos</button>
  		    <div class="dropdown-menu">
  			    <h5 class="dropdown-header">¿Buscas Empleo?</h5>
  			    <a class="dropdown-item" href="#pasantias">Empleo</a>
            <h5 class="dropdown-header">¿O Pasantía?</h5>
            <a class="dropdown-item" href="#pasantias">Pasantías</a></span>
          </div>
        </li>
      </ul>
    </div>

    <ul class="navbar-nav mr-auto"></ul>

    <!--Ingresar-->
    <div class="navbar-nav justify-content-end">
	    <a class="nav-item nav-link mr-5" href="#"><button class="btn" style="font-size: 17px;" data-toggle="modal" data-target="#myModal">Ingresar</button></a>
    </div>

  </div>
</nav>

<!--Secciones-->
<!--Empresa-->
<div id="empresa" class="container-fluid" style="margin-top:30px">
  <h3 class="text-center text-white titulos box">EMPRESA</h3>
  <br>
  <div class="div1">
  	<div class="div2">
  		<div class="div3"><br>

  			<!--Nuestra Empresa-->
  			<div class="row">
  				<div class="col-sm-8">
  					<h2>Nuestra Empresa</h2><hr width="35%" align="left">

  					<p>El Grupo KTS Corp, es una empresa de asesoría y servicios profesionales; que asume el reto de utilizar el conocimiento y la tecnología como el motor principal del cambio, el servicio como fórmula para cumplir una función creando vínculos y la autonomía como la convicción para crear iniciativas innovadoras.</p>

  					<p>KTS Corp, C.A. es una organización integrada por empresas de tecnología, que reúnen talentos profesionales de primera línea, altamente motivados y que brindan una atención excepcional a cada Cliente, dedicados a respaldarlos en el éxito al frente de sus empresas y proyectos.</p>
  				</div>

  				<div class="col-sm-4 text-center">
            <i class="fa fa-building slideanim box" style="font-size: 200px; color: #395784;"></i>
  				</div>
  			</div>

  			<br>
            
        <!--Misión y Visión-->
  			<div class="row">
  				<div class="col-sm-4 text-center">
            <i class="fa fa-globe slideanim box" style="font-size: 200px; color: #395784;"></i>
  				</div>

  				<div class="col-sm-8">
  					<h2>Misión y Visión</h2><hr style="width: 40%"align="left" >
            <h5><strong>MISIÓN:</strong></h5>
            <p>Somos un equipo de líderes con visión estratégica, capaces de desarrollar y fortalecer unidades de negocios exitosas en el área de las tecnologías de información y comunicación en Latinoamérica, generando valor a colaboradores, accionistas y las comunidades donde operamos.</p>

            <h5><strong>VISIÓN:</strong></h5>
            <p>Ser una Corporación reconocida en Latinoamérica por maximizar la rentabilidad de empresas y emprendedores, potenciando sus Modelos de Negocios, a través de la tecnología de información y comunicación, apoyados en formas innovadoras en la gestión del talento humano.</p>
  				</div>
  			</div>

  			<br>

  			<!--Valores-->
  			<h2>Nuestros Valores</h2>
        <hr style="width: 25%;" align="left">

        <div id="valores" class="carousel slide" data-ride="carousel">
          <!--The slideshow-->
          <div class="carousel-inner">

            <div class="carousel-item active">
              <div class="emprendimiento">
                <div class="carousel-caption">
                  <h3>Emprendimiento</h3>
                  <h5>Desarrollar y poner en marcha ideas innovadoras, aprovechando la tecnología y el conocimiento, aplicables tanto para modelos de negocio como para procesos establecidos. Proponer acciones innovadoras que permitan a la organización crecer, desarrollarse y expandirse.</h5>
                </div>
              </div>
            </div>

            <div class="carousel-item">
              <div class="servicio">
                <div class="carousel-caption">
                  <h3>Servicio</h3>
                  <h5>Es el arte de estar a disposición permanente del cliente, interno o externo, proveedores, comunidad y sociedad, indagando sus necesidades, con el fin de satisfacerlas superando sus expectativas.</h5>
                </div>
              </div>
            </div>

            <div class="carousel-item">
              <div class="responsabilidad">
                <div class="carousel-caption">
                  <h3>Responsabilidad</h3>
                  <h5>Capacidad para cumplir compromisos adquiridos y dar respuesta oportuna ante situaciones presentes. Cualidad que implica velar y dedicarse en el cumplimiento de los objetivos, ejecutar las tareas asignadas de acuerdo al rol, cuidando lo que se hace y dice.</h5>
                </div>
              </div>
            </div>

            <div class="carousel-item">
              <div class="compromiso">
                <div class="carousel-caption">
                  <h3>Compromiso</h3>
                  <h5>Capacidad de asumir los retos y responsabilidades con dedicación y lealtad.</h5>
                </div>
              </div>
            </div>

            <div class="carousel-item">
              <div class="respeto">
                <div class="carousel-caption">
                  <h3>Respeto</h3>
                  <h5>Compartir ideas, puntos de vista, opiniones con un enfoque múltiple, en un ambiente de cordialidad y de aceptación a la diversidad.</h5>
                </div>
              </div>
            </div>

            <div class="carousel-item">
              <div class="justicia">
                <div class="carousel-caption">
                  <h3>Justicia</h3>
                  <h5>Actuación equilibrada, trato equitativo y reconocimiento al mérito que promueve la paz organizacional.</h5>
                </div>
              </div>
            </div>

            <div class="carousel-item">
              <div class="trabajo">
                <div class="carousel-caption">
                  <h3>Trabajo en Equipo</h3>
                  <h5>Cualidad de mutua cooperación y flexibilidad, con roles claramente definidos, donde se desarrolla la sinergia para el logro de objetivos comunes.</h5>
                </div>
              </div>
            </div>
          </div>

          <!--Left and right controls-->
          <span class="carousel-control-prev" href="#valores" data-slide="prev">
            <span class="carousel-control fa fa-arrow-circle-left" style="font-size: 20px; color: #848584;"></span>
          </span>
          <span class="carousel-control-next" href="#valores" data-slide="next">
            <span class="carousel-control fa fa-arrow-circle-right" style="font-size: 20px; color: #848584;"></span>
          </span>
        </div>
        <br>
      </div>
    </div>
  </div>

<!--Servicios-->
<br>
<h3 class="text-center text-white titulos box" id="servicios">SERVICIOS</h3>
<br>
<div class="div1">
  <div class="div2">
  	<div class="div3"><br>

  		<div class="row">
        <div class="col-sm-4">
          <center><i class="fa fa-money box" style="font-size: 100px; color: #395784;"></i></center>
          <br>
          <h2 class="text-center">Finanzas</h2>
          <hr>
          <ul class="text-left" style="padding-left: 20px;">
            <li>Revisión y aprobación de flujo de caja.</li>
              <li>Análisis y entrega de estados financieros.</li>
              <li>Monitoreo de cumplimiento de deberes formales.</li>
              <li>Supervisión de las Coordinaciones de administración, soporte técnico y talento humano.</li>
              <li>Asesoría, Consultoría y Auditoría de procesos.</li>
          </ul>
        </div>
        <div class="col-sm-4">
          <center><i class="fa fa-bar-chart box" style="font-size: 100px; color: #395784;"></i></center>
          <br>
          <h2 class="text-center">Control y Gestión</h2>
          <hr>
          <ul class="text-left" style="padding-left: 20px;">
            <li>Seguimiento diario de ventas y cumplimiento de metas.</li>
            <li>Informes periodicos de estadística de ventas y compras.</li>
            <li>Seguimientos de objetivos estratégicos por departamento.</li>
            <li>Prestaciones de resultado de gestión.</li>
          </ul>
        </div>
        <div class="col-sm-4">
          <center><i class="fa fa-cubes box" style="font-size: 100px; color: #395784;"></i></center>
          <br>
          <h2 class="text-center">Procura y Logística</h2>
          <hr>
          <ul class="text-left" style="padding-left: 20px;">
            <li>Análisis de rotación de inventario.</li>
            <li>Procura de productos nacionales e importados.</li>
            <li>Recepción y despacho de mercancía.</li>
            <li>Control de inventario.</li>
          </ul>        
        </div>
      </div>

      <br>

  		<div class="row slideanim">
        <div class="col-sm-4">
          <center><i class="fa fa-calculator box" style="font-size: 100px; color: #395784;"></i></center>
          <br>
          <h2 class="text-center">Administración</h2>
          <hr>
          <ul class="text-left" style="padding-left: 20px;">
            <li>Administración y control de los recursos financieros.</li>
            <li>Gestión de cuentas por cobrar y cuentas por pagar.</li>
            <li>Registro y control de ingresos y egresos.</li>
            <li>Gestión de servicios generales y recursos de mantenimiento.</li>
            </ul>
        </div>
        <div class="col-sm-4">
          <center><i class="fa fa-group box" style="font-size: 100px; color: #395784;"></i></center>
          <br>
          <h2 class="text-center">Talento Humano</h2>
          <hr>
          <ul class="text-left" style="padding-left: 20px;">
            <li>Reclutamiento y selección de personal.</li>
            <li>Administración de beneficios.</li>
            <li>Desarrollo organizacional.</li>
            <li>Seguridad y salud laboral.</li>
            <li>Asesoría, Consultoría y Auditoría de procesos.</li>
          </ul>
        </div>
        <div class="col-sm-4">
          <center><i class="fa fa-wrench box" style="font-size: 100px; color: #395784;"></i></center>
          <br>
          <h2 class="text-center">Soporte Técnico</h2>
          <hr>
          <ul class="text-left" style="padding-left: 20px;">
            <li>Mantenimiento preventivo y correctivo</li>
            <li>Mantenimiento de aplicaciones y software.</li>
            <li>Administración de servidores, bases de datos, redes y software administrativos.</li>
            <li>Administración de requerimientos a nivel de usuarios.</li>
            <li>Control de usuarios y claves de acceso.</li>
            <li>Respaldo de datos.</li>
          </ul>        
        </div>
      </div>
    </div>
  </div>
</div>

<!--Clientes-->
<br>
<h3 class="text-center text-white titulos box" id="clientes">CLIENTES</h3>
<br>
<div class="div1">
  <div class="div2">
    <div class="div3">

      <div class="row text-center">
        <div class="col-sm-4"><br>
        	<img src="img/maxferrer.png" style="width: 190px;"><br><br>
        	<h4>Max Ferrer</h4>
        	<p>Su amigo eléctronico desde 1954</p>
        	<button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#myModal1" role="button" style="font-size: 18px;">Saber Más</button>
        </div>
                      
        <div class="col-sm-4"><br>
          <img src="img/kts.png" class="kts" style="width: 180px; background-color: #848584;"><br><br>
            <h4>KTS Corp</h4>
            <p>Talento y dedicación a su servicio</p>
            <button type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#myModal2" role="button" style="font-size: 18px;">Saber Más</button>
        </div>

        <div class="col-sm-4"><br>
          <img src="img/anatel.png" style="width: 190px;"><br><br>
          <h4>Anatel</h4>
          <p>Toda la red en un solo punto</p>
          <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myModal3" role="button" style="font-size: 18px;">Saber Más</button>
        </div>
      </div>

      <br>

      <div class="row slideanim text-center">
      	<div class="col-sm-4"><br>
      		<img src="img/neutron.png" style="width: 185px;"><br><br>
            <h4>Neutron</h4>
            <p>Más allá de la tecnología</p>
            <button type="button" class="btn btn-outline-dark" data-toggle="modal" data-target="#myModal4" role="button" style="font-size: 18px;">Saber Más</button>
          </div>

          <div class="col-sm-4"><br>
            <img src="img/netdata.png" style="width: 180px;"><br><br>
            <h4>Netdata</h4>
            <p>Redes y soluciones de datos</p>
            <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#myModal5" role="button" style="font-size: 18px;">Saber Más</button>
          </div>

          <div class="col-sm-4"><br>
            <img src="img/mindforce.png" style="width: 230px;"><br><br>
            <h4>Mindforce</h4>
            <p>Tecnología Abierta Mindforce</p>
            <button type="button" class="btn btn-outline-warning" data-toggle="modal" data-target="#myModal6" role="button" style="font-size: 18px;">Saber Más</button>
          </div>
      </div><br>
    </div>
  </div>
</div>

<!--Contáctanos-->
<div id="pasantias"></div>
<h3 class="text-center text-white titulos box">CONTÁCTANOS</h3>
<br>
<div class="div1">
  <div class="div2">
    <div class="div3"><br>

      <div class="row">
      	<div class="col-sm-5 text-center slideanim">
      		<i class="fa fa-address-card box" style="font-size: 200px; color: #395784;"></i>
      	</div>

        <!--Formulario-->
        <div class="col-sm-7">
        	<form action="php/formcontacto.php" id="contactos" name="contactos" method="POST" enctype="multipart/form-data">

            <!--Fila 1-->
            <div class="form-row">
              <div class="form-group col-md-4">
                <!--Nombre-->
                <div class="input-group">
                	<div class="input-group-text"><span class="fa fa-pencil" style="color: #395784;" title="¡Aviso!" data-toggle="popover" data-trigger="hover" data-content="No dejes espacios vacíos en el campo."></span></div>
                	<input type="text" id="nombre" name="nombre" class="form-control" maxlength="16" placeholder="Nombre">
                </div>
              </div>

              <!--Apellido-->
              <div class="form-group col-md-4">
                <div class="input-group">
                  <div class="input-group-text"><span class="fa fa-pencil" style="color: #395784;" title="¡Aviso!" data-toggle="popover" data-trigger="hover" data-content="No dejes espacios vacíos en el campo."></span></div>
                    <input type="text" id="apellido" name="apellido" class="form-control" maxlength="16" placeholder="Apellido">
                  </div>
              </div>

              <!--Edad-->
              <div class="form-group col-md-4">
                <div class="input-group">
                  <div class="input-group-text"><span class="fa fa-calendar-check-o" style="color: #395784;" title="¡Aviso!" data-toggle="popover" data-trigger="hover" data-content="Debes ser mayor de 18 años."></span></div>
                  <select name="opc_edad" id="edad" class="form-control"> 
                    <option value="">Edad</option>
                      <?php for ($i=18; $i <= 60; $i++) { ?>
                    <option value="<?php echo $i ?>"><?php echo $i ?></option>
                      <?php } ?>
                  </select>
                </div>
              </div>
            </div>

            <!--Fila 2-->
            <!--Opciones-->
            <div class="input-group">
              <div class="input-group-text"><span class="fa fa-id-badge" style="color: #395784;" style="font-size: 20px;" title="¡Aviso!" data-toggle="popover" data-trigger="hover" data-content="Cada empresa tiene su propio cargo, no varía si solicitas empleo o pasantías académicas. Elige donde quieres trabajar y en que te especializas."></span></div>
              <select id="trabajo" name="opc_empresa" class="form-control">
                <option value="">Elige una opción</option>
                <option value="1">Finanzas y Contabilidad (KTS Corp)</option>
                <option value="2">Control y Gestión (KTS Corp)</option>
                <option value="3">Administración (KTS Corp - Max Ferrer - Anatel - Netdata)</option>
                <option value="4">Procura y Logística (KTS Corp - Max Ferrer - Anatel)</option>
                <option value="5">Talento Humano (KTS Corp)</option>
                <option value="6">Soporte Técnico (KTS Corp)</option>
                <option value="7">Operaciones e Implementación (Mindforce - Netdat</option>
                <option value="8">Ventas (Max Ferrer - Anatel - Netdata)</option>
              </select>
            </div>

            <br>

            <!--Fila 3-->
            <div class="form-row">
              <!--Correo Electrónico-->
              <div class="form-group col-md-6">
                <div class="input-group">
                  <div class="input-group-text"><span class="fa fa-at" style="color: #395784;"></span></div>
                  <input type="email" id="email" name="email" class="form-control" maxlength="64" placeholder="Dirección de Correo Electrónico">
                 </div>
              </div>

              <!--Número-->
              <div class="form-group col-md-6">
                <div class="input-group">
                  <div class="input-group-text"><span style="color: #395784;" title="¡Aviso!" data-toggle="popover" data-trigger="hover" data-content="Tu número debe contener 7 dígitos"><strong>+58</strong></span></div>
                  <select id="celular" name="num_codigo" class="form-control">
                    <option value="">Código</option>
                    <option value="1">414</option>
                    <option value="2">424</option>
                    <option value="3">416</option>
                    <option value="4">426</option>
                    <option value="5">412</option>
                  </select>
                  <input type="text" id="numero" name="numero" class="form-control" min="0" maxlength="7" placeholder="Número">
                </div>
              </div>
            </div>

            <!--Fila 4-->
            <!--Área de texto-->
            <textarea class="form-control txt-tam" rows="2" id="mensaje" name="txtmensaje" placeholder="¿Por que y donde te gustaría trabajar con nosotros? Puedes escribir tu resumen curríclar en este campo." maxlength="500"></textarea>
            <br>

            <!--Fila 5-->
            <div class="form-row">
              <!--Radio-->
              <div class="form-group col-md-4">
                <label for="radio">Solicito:</label>
                <br>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" id="radio1" name="radio" value="1" checked>
                  <label class="form-check-label" for="radio1">Empleo</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" id="radio2" name="radio" value="2">
                  <label class="form-check-label" for="radio2">Pasantías</label>
                </div>
                <small id="radiomsj" class="form-text "></small>
              </div>

              <br>

              <!--Checkbox-->
              <div class="form-group col-md-4">
                <label for="checkbox">Comunicarse conmigo mediante:</label>
                <?php
    					    // Buscar los Checkbox en la Base de Datos
    					    $cnx->set_charset("utf8");
    					    $querybuscarChk = "SELECT * FROM contacto_checkboxs";
                  $QB = mysqli_query($cnx, $querybuscarChk) or die(mysqli_error($cnx));
                  while (($fila=mysqli_fetch_array($QB)))
    					    {
    						    $idchk = $fila['idchk'];
                    $descripchk = $fila['descripchk'];
                    if($descripchk == 'E-mail'){
                ?>
                <div class="form-check form-check-inline">
                  <input type="checkbox" id="<?php echo $descripchk?>" name="checkbox[]" value="<?php echo $idchk?>" class="form-check-input" title="Sugerencia" data-toggle="popover" data-trigger="hover" data-content="Habilita esta casilla para comunicarnos contigo!" checked>
                  <label class="form-check-label" for="<?php echo $descripchk?>"><?php echo $descripchk ?></label>
                </div>
                <?php
                  }else{
                ?>
                <div class="form-check form-check-inline">
    							<input class="form-check-input" type="checkbox" id="<?php echo $descripchk?>" name="checkbox[]" value="<?php echo $idchk?>">
    							<label class="form-check-label" for="<?php echo $descripchk?>"><?php echo $descripchk ?></label>
                </div>
    						<?php
                  }
                }
    				    ?>
                <small id="chkmsj" class="form-text "></small>
              </div>
              
              <br>

              <!--Archivos-->
              <div class="form-group col-md-4">
                <input type="file" id="archivo" name="archivo" class="form-control-file" accept=".doc,.docx,.pdf" title="Sugerencia" data-toggle="popover" data-trigger="hover" data-content="Adjunta aquí tu Currículum"><small id="archivomsj" class="form-text "></small>
                <span style="color: #848584; font-size: 13px;">Subida 2mb máximo (.docx ó .pdf)</span>
              </div>
            </div>

            <br>
                
            <!--Enviar-->
            <div id="envio">
              <button type="button" name="enviar" id="enviar" class="btn btn-block slideanim">Enviar Datos</button>
            </div>
          </form>
          <br>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!--Maps-->
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3920.9074442244946!2d-71.62021868565058!3d10.66429199239772!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e8998e223704fc7%3A0x8d02f85ebc02727c!2sKTS+Corp!5e0!3m2!1ses-419!2sve!4v1533240988811" width="100%" height=200px frameborder="0" class="slideanim" allowfullscreen></iframe>

<!--Pie de Página-->
<footer class="font-small" style="background-color: #848584;">

  <div style="background-color: #395784;">
    <div class="container text-white">
      <div class="row py-4 d-flex align-items-center">
        <div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">
          <h6 class="mb-0">Talento y Dedicación a su Servicio</h6>
        </div>
        <div class="col-md-6 col-lg-7 text-center text-md-right">
          <!--facebook maxferrer-->
          <a href="https://www.facebook.com/amigomaxferrer" data-toggle="tooltip" title="Facebook de Max Ferrer" target="_blank">
            <i class="fa fa-facebook-square text-danger box" style="font-size:24px; text-decoration: none;"></i>
          </a>
          <!--twitter maxferrer-->
          <a href="https://twitter.com/amigomaxferrer" data-toggle="tooltip" title="Twitter de Max Ferrer" target="_blank">
            <i class="fa fa-twitter-square text-danger box" style="font-size:24px; text-decoration: none;"></i>
          </a>
          <!--instagram maxferrer-->
          <a href="https://www.instagram.com/amigomaxferrer/" data-toggle="tooltip" title="Instagram de Max Ferrer" target="_blank">
            <i class="fa fa-instagram text-danger box" style="font-size:24px; text-decoration: none;"></i>
          </a>
          &nbsp;&nbsp;
          <!--facebook anatel-->
          <a href="https://www.facebook.com/anatelredes/" data-toggle="tooltip" title="Facebook de Anatel" target="_blank">
            <i class="fa fa-facebook-square text-primary box" style="font-size:24px; text-decoration: none;"></i>
          </a>
          <!--twitter anatel-->
          <a href="https://twitter.com/AnatelRedes" data-toggle="tooltip" title="Twitter de Anatel" target="_blank">
            <i class="fa fa-twitter-square text-primary box" style="font-size:24px; text-decoration: none;"></i>
          </a>
          <!--instagram anatel-->
          <a href="https://www.instagram.com/anatelredes/" data-toggle="tooltip" title="Instagram de Anatel" target="_blank">
            <i class="fa fa-instagram text-primary box" style="font-size:24px; text-decoration: none;"></i>
          </a>
          &nbsp;&nbsp;
          <!--facebook netdata-->
          <a href="https://www.facebook.com/netdatanetworks/" data-toggle="tooltip" title="Facebook de Netdata" target="_blank">
            <i class="fa fa-facebook-square text-success box" style="font-size:24px; text-decoration: none;"></i>
          </a>
          <!--instagram netdata-->
          <a href="https://www.instagram.com/netdatanetworks/" data-toggle="tooltip" title="Instagram de Netdata" target="_blank">
            <i class="fa fa-instagram text-success box" style="font-size:24px; text-decoration: none;"></i>
          </a>
        </div>
      </div>
    </div>
  </div>
    
  <!--contenido-->
  <div class="container text-center text-md-left mt-5 text-white">
    <div class="row mt-3">
      <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
        <!--KTS Corp-->
        <h6 class="text-uppercase font-weight-bold">KTS Corp C.A.</h6>
        <hr class="accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
        <p>Es una organización integrada por empresas de tecnología, que reúnen talentos profesionales que brindan una atención excepcional a cada Cliente, dedicados a respaldarlos en el éxito al frente de sus empresas y proyectos.</p>
      </div>

      <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
        <!--Acerca De-->
        <h6 class="text-uppercase font-weight-bold">Acerca De</h6>
        <hr class="accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
        <p><a href="legal/politicas_de_privacidad.html" target="_blank" style="color: #fff;">Políticas de Privacidad</a></p>
        <p><a href="legal/terminos_condiciones.html" target="_blank" style="color: #fff;">Términos y Condiciones de Uso</a></p>
      </div>

      <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
        <!--Clientes-->
        <h6 class="text-uppercase font-weight-bold">Clientes</h6>
        <hr class="accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
        <p><a href="http://anatel.com.ve/" target="_blank" style="color: #fff;">Anatel C.A.</a></p>
        <p><a href="http://www.maxferrer.com.ve/" target="_blank" style="color: #fff;">Max Ferrer C.A.</a></p>
        <p><a href="#clientes" style="color: #fff;">MindForce C.A.</a></p>
        <p><a href="http://netdata.com.ve/inicio.php" target="_blank" style="color: #fff;">Netdata C.A.</a></p>
        <p><a href="#clientes" style="color: #fff;">Neutron C.A.</a></p>
      </div>

      <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
        <!--Contacto-->
        <h6 class="text-uppercase font-weight-bold">Contacto</h6>
        <hr class="accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
        <p><i class="fa fa-home mr-3"></i>Calle 77 entre Av.14A y 15. Edificio 5 de julio, oficina A-04</p>
        <p><i class="fa fa-envelope mr-3"></i>talentohumano@ktscorp.com</p>
        <p><i class="fa fa-phone mr-3"></i>+58 2617987196</p>
      </div>
    </div>
  </div>

  <!--Copyright-->
  <div class="text-center py-3 text-white" style="background-color: #395784;">© <span id="output"></span> | KTS Corp - Todos los derechos reservados | RIF J-29454989-6</div>

</footer>

<!--Modales-->
<!--Ingresar-->
<div class="modal fade" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <!--Modal Header-->
      <div class="modal-header">
        <h4 class="modal-title">¡Alerta! Solo personal autorizado <i class="fa fa-warning" style="font-size:20px; color: #ffae42;"></i></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!--Modal body-->
      <div class="modal-body">
      <h5>Uso Exclusivo del personal de KTS Corp. y sus empresas aliadas <i class="fa fa-group" style="font-size:20px; color: #395784;"></i></h5>
      </div>
      <!--Modal footer-->
      <div class="modal-footer">
        <a href="php/ingresar.php"><button type="button" class="btn">Ingresar</button></a>
      </div>
    </div>
  </div>
</div>

<!--Max Ferrer-->
<div class="modal fade" id="myModal1">
  <div class="modal-dialog">
    <div class="modal-content">
      <!--Modal Header-->
      <div class="modal-header">
        <h4 class="modal-title">Max Ferrer</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!--Modal body-->
      <div class="modal-body">
        <h5>Nuestra empresa es bien conocida por sus precios convenientes, calidad de productos y atención personalizada al cliente.
        <br><br>
        Nos caracterizamos por ser una empresa pujante, que incorpora de manera constante novedades y diversas opciones para cubrir las necesidades de cada cliente y los mejores precios, sumado a un excelente servicio.</h5>
      </div>
      <!--Modal footer-->
      <div class="modal-footer">
        <a href="http://www.maxferrer.com.ve/" target="_blank"><button type="button" class="btn btn-outline-danger">Visitar</button></a>
      </div>
    </div>
  </div>
</div>

<!--KTS Corp-->
<div class="modal fade" id="myModal2">
  <div class="modal-dialog">
    <div class="modal-content">
      <!--Modal Header-->
      <div class="modal-header">
        <h4 class="modal-title">KTS Corp</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!--Modal body-->
      <div class="modal-body">
        <h6>Las cinco empresas están vinculadas por su composición accionaria, y operacionalmente, por:
        <br><br>
        <i class="fa fa-check" style="font-size:20px; color: #395784;"></i> Una misma filosofía y estilo gerencial autónomo orientado a &nbsp;&nbsp;&nbsp;&nbsp; la productividad.<br>
        <i class="fa fa-check" style="font-size:20px; color: #395784;"></i> La combinación del conocimiento, la tecnología y el &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;servicio, para la generación de iniciativas  exitosas.<br>
        <i class="fa fa-check" style="font-size:20px; color: #395784;"></i> La complementariedad de sus negocios o nichos de &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mercado.<br>
        <i class="fa fa-check" style="font-size:20px; color: #395784;"></i> Su potencial de crecimiento.
        <br><br>
        Nuestras empresas operan en el mercado nacional e Internacional con:
        <br><br>
        <i class="fa fa-check" style="font-size:20px; color: #395784;"></i> Proyectos de expansión progresiva a otras regiones.<br>
        <i class="fa fa-check" style="font-size:20px; color: #395784;"></i> Conexión internacional para garantizar los suministros.<br>
        <i class="fa fa-check" style="font-size:20px; color: #395784;"></i> Propósitos de expansión a otras localidades internacionales.</h6>
      </div>
      <!--Modal footer-->
      <div class="modal-footer">
        <a href=""><button type="button" class="btn btn-outline-secondary"  data-dismiss="modal">Cerrar</button></a>
      </div>
    </div>
  </div>
</div>

<!--Anatel-->
<div class="modal fade" id="myModal3">
  <div class="modal-dialog">
    <div class="modal-content">
      <!--Modal Header-->
      <div class="modal-header">
        <h4 class="modal-title">Anatel</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!--Modal body-->
      <div class="modal-body">
        <h5>Una red de tiendas al mayor y detal a nivel Nacional e Internacional, basadas en el concepto “One Stop Networking” toda la red en un solo punto: 
        <br><br>
        Anatel constituye un espacio físico y de relación, donde el cliente tiene a su disposición todo lo que pueda requerir para desarrollar, instalar y mantener una red de Voz, Data y Video, alámbrica o inalámbrica.</h5>
      </div>
      <!--Modal footer-->
        <div class="modal-footer">
        <a href="http://anatel.com.ve/" target="_blank"><button type="button" class="btn btn-outline-primary">Visitar</button></a>
      </div>
    </div>
  </div>
</div>

<!--Neutron-->
<div class="modal fade" id="myModal4">
  <div class="modal-dialog">
    <div class="modal-content">
      <!--Modal Header-->
      <div class="modal-header">
        <h4 class="modal-title">Neutron</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!--Modal body-->
      <div class="modal-body">
        <h5>Neutron C.A. es una empresa Venezolana con mas de 26 años de experiencia en el mercado venezolano, la cual se dedica a proveer soluciones el área de informática y telecomunicaciones con personal altamente calificado.</h5>
      </div>
      <!--Modal footer-->
      <div class="modal-footer">
        <a href=""><button type="button" class="btn btn-outline-dark" data-dismiss="modal">Cerrar</button></a>
      </div>
    </div>
  </div>
</div>

<!--Netdata-->
<div class="modal fade" id="myModal5">
  <div class="modal-dialog">
    <div class="modal-content">
      <!--Modal Header-->
      <div class="modal-header">
        <h4 class="modal-title">Netdata</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!--Modal body-->
      <div class="modal-body">
        <h5>Netdata es una  empresa proveedora de Soluciones de Redes IP con personal altamente calificado que apoya a sus clientes en: Planificación, Diseño, Implementación, Migración, Operación y Optimización.
        <br><br> 
        Nuestra estrategia busca aumentar sus ingresos a través de la prestación de nuevos y mejores servicios, optimizar sus costos operativos, y elevar los niveles de satisfacción y lealtad de los clientes.</h5>
      </div>
      <!--Modal footer-->
      <div class="modal-footer">
        <a href="http://netdata.com.ve/inicio.php" target="_blank"><button type="button" class="btn btn-outline-success">Visitar</button></a>
      </div>
    </div>
  </div>
</div>

<!--Mindforce-->
<div class="modal fade" id="myModal6">
  <div class="modal-dialog">
    <div class="modal-content">
      <!--Modal Header-->
      <div class="modal-header">
        <h4 class="modal-title">Mindforce</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!--Modal body-->
      <div class="modal-body">
        <h5>Líder en Venezuela de soluciones de múltiples proveedores en el mercado de infraestructura de comunicaciones. La compañía ha aumentado en alcance y profundidad, evolucionando en respuesta a los cambios del mercado. Con sede en Caracas Venezuela.
        <br><br>
        La empresa suministra a los principales fabricantes de equipos y operadores móviles con una gama completa de servicios modulares. Esto incluye el suministro, outsourcing de procura y diseño, instalación y puesta en servicio de redes de telecomunicaciones</h5>
      </div>
      <!--Modal footer-->
      <div class="modal-footer">
        <a href=""><button type="button" class="btn btn-outline-warning" data-dismiss="modal">Cerrar</button></a>
      </div>
    </div>
  </div>
</div>

<!--JS-->
<script src="js/funciones.js"></script>
<script src="js/contacto.js"></script>

</body>
</html>