<?php
	session_start();
	if(isset($_SESSION['autentificado'])){
		if($_SESSION['autentificado'] == true){

			if($_SESSION['opc_user'] == 'usuarios_admin')
			{
				header("Location: paneladmin.php");
			}elseif ($_SESSION['opc_user'] == 'usuarios') 
			{
				header("Location: paneluser.php");
			}
		}
	}else{
?>

<!DOCTYPE html>
<html lang="es">
<head>
<!--Metadatos y Datos-->
<title>KTS Corp - Ingresar</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="image/x-icon" href="../img/icono.png">

<!--Local-->
<link rel="stylesheet" type="text/css" href="../css/sesion.css">
<link rel="stylesheet" type="text/css" href="../css/estilos.css">

<!--Bootstrap-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

<!--Iconos-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

<!--Símbolo de carga-->
<div id="contenedor_loader">
	<div class="loader" id="loader"></div>
</div>

<!--Formulario de ingreso-->
<form action="formingresar.php" name="ingresar" method="POST">

<!--Logo-->
<img src="../img/kts.png" class="img-fluid">

<!--Cedula-->
<div class="input-group">
	<div class="input-group-text">
		<span class="fa fa-user-circle" style="color: #395784;"></span>
	</div>
	<input type="text" id="cedula" name="cedula" class="form-control" placeholder="Cédula">
</div>

<br>

<!--Contraseña-->
<div class="input-group">
	<div class="input-group-text">
		<span class="fa fa-lock" style="color: #395784; font-size: 22px;"></span>
	</div>
	<input type="password" id="contraseña" name="contraseña" class="form-control" placeholder="Contraseña" maxlength="16">
</div>

<br>

<!--Tipo de Usuario-->
<div class="input-group">
	<div class="input-group-text">
		<span class="fa fa-user-circle" style="color: #395784;"></span>
	</div>
	<select class="form-control" id="opctipo" name="opc_user">
		<option value="">Tipo de Usuario</option>
		<option value="usuarios_admin">Administrador</option>
		<option value="usuarios">Empleado</option>
	</select>
</div>

<br>

<label>Necesitas una cuenta? Comunicate con el administrador de KTS Corp. para mayor información.</label>

<br>

<!--Continuar-->
<div id="enviar">
	<button type="button" name="continuar" id="continuar" class="btn btn-block">Iniciar Sesion</button>
</div>

<br>

<!--Regresar-->
<a href="../index.php" style="text-decoration: none;"><button type="button" class="btn btn-block">Regresar</button></a>
</form>

<!--JS-->
<script src="../js/funciones.js"></script>
<script src="../js/ingresar.js"></script>

</body>
</html>
<?php
	}
?>