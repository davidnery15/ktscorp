<?php
  include_once('conexion/cnx.php');  //Agregando la Conexión a Base de Datos
  include_once('sesion.php');        //Agregando la Verificación de Sesión
?>
<!DOCTYPE html>
<html lang="es">
<head>
<!--Metadatos y Datos-->
<title>KTS Corp - Mostrar Usuarios</title>
<meta charset="utf-8">
<meta name="KTS Corp" content="Extranet">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="image/x-icon" href="../img/icono.png">

<!--Local-->
<link rel="stylesheet" type="text/css" href="../css/estilos.css">
<link rel="stylesheet" type="text/css" href="../css/sidebar.css">

<!--Bootstrap-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

<!--Iconos-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

<!--Símbolo de Carga-->
<div id="contenedor_loader">
  <div class="loader" id="loader"></div>
</div>

<!--Menú-->
<?php
  include_once('menuadmin.php');
?>

<div id="content-wrapper">
  <div class="container-fluid">
    
    <div class="div1">
      <div class="div2">
        <div class="div3">
          <br>
          <div class="row">
            <div class="col-sm-6">
              <form class="text-center">
                <div class="input-group">
                  <div class="input-group-text">
                    <span class="fa fa-search" style="color: #395784;"></span>
                  </div>
                  <input type="text" class="form-control" placeholder="Buscar Usuario" aria-label="Search" aria-describedby="basic-addon2">
                </div>
              </form>
            </div>
            <div class="col-sm-6 text-center">
              <code>Buscar usuarios por: Nombre, Apellido, Cédula, Cargo y Compañia.</code>
            </div>
          </div>
          <hr>
          <!--Mostrar Usuarios Administradores-->
          <div class="container text-center"><br>
            <h4 class="text-white titulos box">Administradores</h4><br>
            <?php
              $querybuscarC = "SELECT * FROM usuarios_admin";
              $QB = mysqli_query($cnx, $querybuscarC) or die(mysqli_error($cnx));
              if (mysqli_num_rows($QB) > 0 )
              {
            ?>
            <div class="container table-responsive">
              <table class="table table-hover table-bordered">
                <thead>
                  <tr>
                    <th><strong>N°</strong></th>
                    <th><strong>Nombre</strong></th>
                    <th><strong>Apellido</strong></th>
                    <th><strong>Cédula</strong></th>
                    <th><strong>Cargo</strong></th>
                    <th><strong>Compañia</strong></th>
                    <th><strong>Modificar</strong></th>
                    <th><strong>Eliminar</strong></th>
                  </tr>
                </thead>
                <?php
                  $nro = 0;
                  while( $fila=mysqli_fetch_array($QB) )
                  {
                    $nro++;
                    $idadmin = $fila['idadmin'];
                    $nombre = $fila['nombre'];
                    $apellido = $fila['apellido'];
                    $cedula = $fila['cedula'];
                    $idcargo = $fila['idcargo'];
                    $idempresa = $fila['idempresa'];
                      
                    $cnx->set_charset("utf8");
          				  $querybuscarOP = " SELECT * FROM usuarios_cargo WHERE idcargo = '$idcargo' ";
                    $QO = mysqli_query($cnx, $querybuscarOP) or die(mysqli_error($cnx));
                    while (($fila=mysqli_fetch_array($QO)))
          					{
                      $descripcargo= $fila['descripcargo'];
                    }
                      
          				  $querybuscarEM = " SELECT * FROM usuarios_empresa WHERE idempresa = '$idempresa' ";
                    $QEM = mysqli_query($cnx, $querybuscarEM) or die(mysqli_error($cnx));
                    while (($fila=mysqli_fetch_array($QEM)))
          					{
          					  $descripempresa= $fila['descripempresa'];
          					}
                ?>
                <tbody id="datos">
                  <tr>
                    <td><?php echo $nro ?></td>
                    <td><?php echo $nombre ?></td>
                    <td><?php echo $apellido ?></td>
                    <td><?php echo $cedula ?></td>
                    <td><?php echo $descripcargo ?></td>
                    <td><?php echo $descripempresa ?></td>
                    <td><a class="btn" href="modificaradmin.php?id=<?php echo $idadmin ?>"><span class="fa fa-pencil"></span></a></td>
                    <td><a class="btn" href="#" onclick="preguntaradmin(<?php echo $idadmin ?>)"><span class="fa fa-user-times"></span></a></td>
                  </tr>
                </tbody>
                <?php
                  }
                ?>
              </table>
              <?php
                }
              ?>
            </div>
          </div>

          <hr>

          <!--Mostrar Usuarios Empleados-->
          <div class="container text-center"><br>
            <h4 class="text-white titulos box">Empleados</h4><br>
            <?php
              $querybuscarC = "SELECT * FROM usuarios";
              $QB = mysqli_query($cnx, $querybuscarC) or die(mysqli_error($cnx));
              if (mysqli_num_rows($QB) > 0 )
              {
            ?>
            <div class="container table-responsive">
              <table class="table table-hover table-bordered">
                <thead>
                  <tr>
                    <td><strong>N°</strong></td>
                    <td><strong>Nombre</strong></td>
                    <td><strong>Apellido</strong></td>
                    <td><strong>Cédula</strong></td>
                    <td><strong>Cargo</strong></td>
                    <td><strong>Compañia</strong></td>
                    <td><strong>Modificar</strong></td>
                    <td><strong>Eliminar</strong></td>
                  </tr>
                </thead>
                <?php
                  $nro = 0;
                  while( $fila=mysqli_fetch_array($QB) )
                  {
                    $nro++;
                    $idusuario = $fila['idusuario'];
                    $nombre = utf8_decode($fila['nombre']);
                    $apellido = utf8_decode($fila['apellido']);
                    $cedula = $fila['cedula'];
                    $idcargo = $fila['idcargo'];
                    $idempresa = $fila['idempresa'];
                      
                    $cnx->set_charset("utf8");
          				  $querybuscarOP = " SELECT * FROM usuarios_cargo WHERE idcargo = '$idcargo' ";
                    $QO = mysqli_query($cnx, $querybuscarOP) or die(mysqli_error($cnx));
                    while (($fila=mysqli_fetch_array($QO)))
          					{
          						$descripcargo= $fila['descripcargo'];
                    }
                      
          				  $querybuscarEM = " SELECT * FROM usuarios_empresa WHERE idempresa = '$idempresa' ";
                    $QEM = mysqli_query($cnx, $querybuscarEM) or die(mysqli_error($cnx));
                    while (($fila=mysqli_fetch_array($QEM)))
          					{
          					  $descripempresa= $fila['descripempresa'];
          					}
                ?>
                <tbody id="datos">
                  <tr>
                    <td><?php echo $nro ?></td>
                    <td><?php echo $nombre ?></td>
                    <td><?php echo $apellido ?></td>
                    <td><?php echo $cedula ?></td>
                    <td><?php echo $descripcargo ?></td>
                    <td><?php echo $descripempresa ?></td>
                    <td><a class="btn" href="modificaruser.php?id=<?php echo $idusuario ?>"><span class="fa fa-pencil"></span></a></td>
                    <td><a class="btn" href="#" onclick="preguntaruser(<?php echo $idusuario ?>)"><span class="fa fa-user-times"></span></a></td>
                  </tr>
                </tbody>
                <?php
                  }
                ?>
              </table>
              <?php
                }else{
              ?>
              <h4>POR LOS MOMENTOS NO HAY NINGÚN USUARIO DE EMPLEADO</h4>
              <?php
                }
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <footer class="sticky-footer">
      <div class="container my-auto">
        <div class="copyright text-center my-auto">
          © <span id="output"></span> | KTS Corp - Todos los derechos reservados | RIF J-29454989-6
        </div>
      </div>
    </footer>
  </div>
</div>

<!--JS-->
<script src="../js/funciones.js"></script>
<script src="../js/sidebar.js"></script>
<script>
  function preguntaradmin(idadmin){
    if(confirm('¿Está seguro que desea eliminar el usuario seleccionado?'))
    {
      window.location.href = "eliminaradmin.php?id="+idadmin; 
    }
  }

  function preguntaruser(idusuario){
    if(confirm('¿Está seguro que desea eliminar el usuario seleccionado?'))
    {
      window.location.href = "eliminaruser.php?id="+idusuario; 
    }
  }
</script>


</body>
</html>