<?php
  include_once('conexion/cnx.php');  //Agregando la Conexión a Base de Datos
  include_once('sesion.php');        //Agregando la Verificación de Sesión

  $idusuario = $_REQUEST['id'];

  $queryInsertarD = " SELECT * FROM usuarios where idusuario = '$idusuario' ";
  $QI1 = mysqli_query($cnx, $queryInsertarD) or die(mysqli_error($cnx));
  while ( $fila=mysqli_fetch_array($QI1) )
  {
 	$nombre = $fila['nombre'];
	$apellido = $fila['apellido'];
	$cedula = $fila['cedula'];
	$cargo = $fila['idcargo'];
	$empresa = $fila['idempresa'];
	$contraseña = $fila['clave'];
  }
?>
<!DOCTYPE html>
<html lang="es">
<head>
<!--Metadatos y Datos-->
<title>Registrar - KTS Corp</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="image/x-icon" href="../img/icono.png">

<!--Local-->
<link rel="stylesheet" type="text/css" href="../css/sesion.css">
<link rel="stylesheet" type="text/css" href="../css/estilos.css">

<!--Bootstrap-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

<!--Iconos-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

<!--Símbolo de carga-->
<div id="contenedor_loader">
	<div class="loader" id="loader"></div>
</div>

<!--Formulario de resgistro-->
<form action="formmodificaruser.php?id=<?php echo $idusuario ?>" name="registro" method="POST">

<!--Logo-->
<img src="../img/kts.png" class="img-fluid">

<!--Nombre-->
<div class="input-group">
	<div class="input-group-text">
		<span class="fa fa-pencil" style="color: #395784;" title="¡Aviso!" data-toggle="popover" data-trigger="hover" data-content="No dejes espacios vacíos en el campo."></span>
	</div>
	<input type="text" id="nombre" name="nombre" class="form-control" placeholder="Nombre" value="<?php echo $nombre ?>">
</div>

<br>

<!--Apellido-->
<div class="input-group">
	<div class="input-group-text">
		<span class="fa fa-pencil" style="color: #395784;" title="¡Aviso!" data-toggle="popover" data-trigger="hover" data-content="No dejes espacios vacíos en el campo."></span>
	</div>
	<input type="text" id="apellido" name="apellido" class="form-control" placeholder="Apellido" value="<?php echo $apellido ?>">
</div>

<br>

<!--Cedula-->
<div class="input-group">
	<div class="input-group-text">
		<span class="fa fa-user-circle" style="color: #395784;" title="¡Aviso!" data-toggle="popover" data-trigger="hover" data-content="No dejes espacios vacíos en el campo."></span>
    </div>
	<input type="text" id="cedula" name="cedula" class="form-control" placeholder="Cedula" value="<?php echo $cedula ?>">
</div>

<br>

<!--Contraseña-->
<div class="input-group">
	<div class="input-group-text">
		<span class="fa fa-lock" style="color: #395784; font-size: 22px;" title="¡Aviso!" data-toggle="popover" data-trigger="hover" data-content="La contraseña tiene una longitud de mínimo 8 y máximo 16 caracteres, la contraseña puede contener estos caracteres espaciales (?/.+@#%)."></span>
	</div>
	<input type="password" id="clave" name="contraseña" class="form-control" placeholder="Contraseña" minlength="8" maxlength="16" value="<?php echo $contraseña ?>">
</div>

<br>

<div class="input-group">
	<!--Cargo-->
	<div class="input-group-text"><span class="fa fa-id-badge" style="color: #395784;" style="font-size: 20px;" title="¡Aviso!" data-toggle="popover" data-trigger="hover" data-content="Selecciona el cargo del empleado."></span></div>
    <select id="cargo" name="cargo" class="form-control">
        <option value="">Elige un Cargo</option>
        <?php
			$cnx->set_charset("utf8");
			$querybuscarO = "SELECT * FROM usuarios_cargo";
			$QBO = mysqli_query($cnx, $querybuscarO) or die(mysqli_error($cnx));
			while (($fila=mysqli_fetch_array($QBO)))
			{
				$idcargo= $fila['idcargo'];
				$descripcargo= $fila['descripcargo'];
				echo "<option value='$idcargo' ";
				if($idcargo == $cargo )
				{
					echo "selected";
				}
				echo ">$descripcargo</option>";
			}
		?>
    </select>
	<!--Empresa-->
    <div class="input-group-text"><span class="fa fa-id-badge" style="color: #395784;" style="font-size: 20px;" title="¡Aviso!" data-toggle="popover" data-trigger="hover" data-content="Selecciona el cargo del empleado."></span></div>
    <select id="empresa" name="empresa" class="form-control">
        <option value="">Elige una Empresa</option>
        <?php 
			$cnx->set_charset("utf8");
			$querybuscarO = "SELECT * FROM usuarios_empresa";
			$QBO = mysqli_query($cnx, $querybuscarO) or die(mysqli_error($cnx));
			while (($fila=mysqli_fetch_array($QBO)))
			{
				$idempresa= $fila['idempresa'];
				$descripempresa= $fila['descripempresa'];
				echo "<option value='$idempresa' ";
				if($idempresa == $empresa )
				{
					echo "selected";
				}
				echo ">$descripempresa</option>";
			}
		?>
    </select>
</div>

<br>

<!--Modificar-->
<div id="enviar">
	<button type="button" name="modificar" id="registrar" class="btn btn-block">Modificar</button>
</div>

<br>

<!--Regresar al Panel-->
<a href="usuarios.php" style="text-decoration: none;"><button type="button" class="btn btn-block">Regresar</button></a>

</form>

<!--JS-->
<script src="../js/funciones.js"></script>
<script src="../js/usuariosregmod.js"></script>

</body>
</html>