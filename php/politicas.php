<?php
  include_once('conexion/cnx.php');  //Agregando la Conexión a Base de Datos
  include_once('sesion.php');        //Agregando la Verificación de Sesión
?>
<!DOCTYPE html>
<html lang="es">
<head>
<!--Metadatos y Datos-->
<title>KTS Corp - Políticas</title>
<meta charset="utf-8">
<meta name="KTS Corp" content="Extranet">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="image/x-icon" href="../img/icono.png">

<!--Local-->
<link rel="stylesheet" type="text/css" href="../css/estilos.css">
<link rel="stylesheet" type="text/css" href="../css/sidebar.css">

<!--Bootstrap-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

<!--Iconos-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

<!--Símbolo de Carga-->
<div id="contenedor_loader">
  <div class="loader" id="loader"></div>
</div>

<!--Menú-->
<?php
  if($_SESSION['opc_user'] == 'usuarios_admin'){
    include_once('menuadmin.php');
  }elseif($_SESSION['opc_user'] == 'usuarios'){
    include_once('menuuser.php');
  }
?>

<div id="content-wrapper">
  <div class="container-fluid text-center">
    <h2 class="text-center text-white titulos box">Políticas</h2>
    <div class="div1">
      <div class="div2">
        <div class="div3"><br>
          
          <div class="row">
            <div class="col-lg-6"><br>
              <i class="fa fa-handshake-o box" style="font-size: 100px; color: #395784;"></i>
              <br>
              <h3 class="text-center">Políticas de Prestaciones</h3>
              <hr style="background-color: #848584">
              <p>En esta sección se encuentran las políticas de prestaciones, en el cuál se conoce su <strong>propósito y alcance, definiciones, documentos asociados, responsables directos del cumpliminento de este procedimiento, políticas de prestaciones sociales y su procedimiento.</strong></p>
              <a href="../legal/politicas_de_prestaciones.html" target="_blank"><button type="button" class="btn" style="font-size: 18px;">Saber Más</button></a>
            </div>
            <div class="col-lg-6"><br>
              <i class="fa fa-male box" style="font-size: 100px; color: #395784;"></i>
              <br>
              <h3 class="text-center">Políticas de Recomendación</h3>
              <hr style="background-color: #848584">
              <p>En esta sección se encuentran las políticas de recomendación, en el cuál se conoce su <strong>propósito y alcance, definiciones, documentos asociados, responsables directos del cumpliminento de este procedimiento y su procedimiento.</strong></p>
              <a href="../legal/politicas_de_recomendacion.html" target="_blank"><button type="button" class="btn" style="font-size: 18px;">Saber Más</button></a>    
            </div>
          </div>

          <br>

          <div class="row slideanim">
            <div class="col-lg-6"><br>
              <i class="fa fa-bus box" style="font-size: 100px; color: #395784;"></i>
              <br>
              <h3 class="text-center">Políticas de Bono de Transporte</h3>
              <hr style="background-color: #848584">
              <p>En esta sección se encuentran las políticas de transporte, en el cuál se conoce su <strong>propósito y alcance, definiciones, documentos asociados, responsables directos del cumpliminento de este procedimiento y su procedimiento.</strong></p>
              <a href="../legal/politicas_de_transporte.html" target="_blank"><button type="button" class="btn" style="font-size: 18px;">Saber Más</button></a>
            </div>
            <div class="col-lg-6"><br>
              <i class="fa fa-plane box" style="font-size: 100px; color: #395784;"></i>
              <br>
              <h3 class="text-center">Políticas de Vacaciones</h3>
              <hr style="background-color: #848584">
              <p>En esta sección se encuentran las políticas de vacaciones, en el cuál se conoce su <strong>propósito y alcance, definiciones, documentos asociados, responsables directos del cumpliminento de este procedimiento, políticas de vacaciones, bono vacacional, disfrute fraccionado de vacaciones y su procedimiento.</strong></p>
              <a href="../legal/politicas_de_vacaciones.html" target="_blank"><button type="button" class="btn" style="font-size: 18px;">Saber Más</button></a>
            </div>
          </div>
          <br>
        </div>
      </div>
    </div>
    <footer class="sticky-footer">
      <div class="container my-auto">
        <div class="copyright text-center my-auto">
          © <span id="output"></span> | KTS Corp - Todos los derechos reservados | RIF J-29454989-6
        </div>
      </div>
    </footer>
  </div>
</div>

<!--JS-->
<script src="../js/funciones.js"></script>
<script src="../js/sidebar.js"></script>

</body>
</html>