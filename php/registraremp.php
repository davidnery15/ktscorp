<?php
  include_once('conexion/cnx.php');  //Agregando la Conexión a Base de Datos
  include_once('sesion.php');        //Agregando la Verificación de Sesión
?>
<!DOCTYPE html>
<html lang="es">
<head>
<!--Metadatos y Datos-->
<title>KTS Corp - Registrar</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="image/x-icon" href="../img/icono.png">

<!--Local-->
<link rel="stylesheet" type="text/css" href="../css/sesion.css">
<link rel="stylesheet" type="text/css" href="../css/estilos.css">

<!--Bootstrap-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

<!--Iconos-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

<!--Símbolo de carga-->
<div id="contenedor_loader">
	<div class="loader" id="loader"></div>
</div>

<!--Formulario de resgistro-->
<form action="formregistroemp.php" name="registro" method="POST">

<!--Logo-->
<img src="../img/kts.png" class="img-fluid" style="margin-top: -20px;">

<!--Nombre-->
<div class="input-group">
	<div class="input-group-text">
		<span class="fa fa-pencil" style="color: #395784;" title="¡Aviso!" data-toggle="popover" data-trigger="hover" data-content="No dejes espacios vacíos en el campo."></span>
	</div>
	<input type="text" id="nombre" name="nombre" class="form-control" placeholder="Nombre">
</div>

<br>

<!--Apellido-->
<div class="input-group">
	<div class="input-group-text">
		<span class="fa fa-pencil" style="color: #395784;" title="¡Aviso!" data-toggle="popover" data-trigger="hover" data-content="No dejes espacios vacíos en el campo."></span>
	</div>
	<input type="text" id="apellido" name="apellido" class="form-control" placeholder="Apellido">
</div>

<br>

<!--Cédula-->
<div class="input-group">
	<div class="input-group-text">
		<span class="fa fa-user-circle" style="color: #395784;" title="¡Aviso!" data-toggle="popover" data-trigger="hover" data-content="No dejes espacios vacíos en el campo."></span>
    </div>
	<input type="text" id="cedula" name="cedula" class="form-control" placeholder="Cédula"maxlength="8" onkeyup="muestraTipeo(this.value)">
</div>
<span id="txtHint" style="margin-left: 60px; color:#fff; "></span>

<script>
    function muestraTipeo(str) {
        if (str.length == 0) {
            document.getElementById("txtHint").innerHTML = "";
            return;
        }else{
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("txtHint").innerHTML = this.responseText;
                }
            };
        xmlhttp.open("GET", "verificar_cedula_user.php?ver_cedula=" + str, true);
        xmlhttp.send();
        }
    }
</script>

<br>

<!--Contraseña-->
<div class="input-group">
	<div class="input-group-text">
		<span class="fa fa-lock" style="color: #395784; font-size: 22px;" title="¡Aviso!" data-toggle="popover" data-trigger="hover" data-content="La contraseña tiene una longitud de mínimo 8 y máximo 16 caracteres, la contraseña puede contener estos caracteres espaciales (?/.+@#%)."></span>
	</div>
	<input type="password" id="clave" name="contraseña" class="form-control" placeholder="Contraseña" minlength="8" maxlength="16">
</div>

<br>

<!--Cargo-->
<div class="input-group">
    <div class="input-group-text"><span class="fa fa-id-badge" style="color: #395784;" style="font-size: 20px;" title="¡Aviso!" data-toggle="popover" data-trigger="hover" data-content="Selecciona el cargo del empleado."></span></div>
    <select id="cargo" name="cargo" class="form-control">
        <option value="">Elige un Cargo</option>
        <option value="1">Administración</option>
        <option value="2">Finanzas y Contabilidad</option>
        <option value="3">Talento Humano</option>
        <option value="4">Operaciones e Implementación</option>
        <option value="5">Ventas</option>
        <option value="6">Control y Gestión</option>
        <option value="7">Procura y Logística</option>
        <option value="8">Soporte Técnico</option>
    </select>
    <!--Empresa-->
    <div class="input-group-text"><span class="fa fa-building" style="color: #395784;" style="font-size: 20px;" title="¡Aviso!" data-toggle="popover" data-trigger="hover" data-content="Selecciona la Empresa donde trabaja el empleado."></span></div>
    <select id="empresa" name="empresa" class="form-control">
        <option value="">Elige una Empresa</option>
        <option value="1">Anatel</option>
        <option value="2">KTS Corp</option>
        <option value="3">Max Ferrer</option>
        <option value="4">Mindforce</option>
        <option value="5">Netdata</option>
        <option value="6">Neutron</option>
    </select>
</div>

<br>

<!--Registrar-->
<div id="enviar">
	<button type="button" name="registrar" id="registrar" class="btn btn-block">Registrar</button>
</div>

<br>

<!--Regresar al Panel-->
<a href="usuarios.php" style="text-decoration: none;"><button type="button" class="btn btn-block">Regresar</button></a>

</form>

<!--JS-->
<script src="../js/funciones.js"></script>
<script src="../js/usuariosregmod.js"></script>

</body>
</html>