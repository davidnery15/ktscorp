$(function(){

	$('#nombre').on({
		focusin: function(){

			$(this).removeClass( 'border-success border-warning border-danger' );
		},
		focusout: function(){
			var elementoT = document.formulario.nombre;
			var re = /^[A-Za-zñÑÁÉÍÓÚáéíóúÿ]{2,}$/i;
			if( elementoT.value === '' )
			{
				$(this).addClass( 'border-warning' );
				co = 1;
			}
			else
			{
				if ( !re.test(formulario.nombre.value) )
				{
					$(this).addClass( 'border-danger' );
					errorTxt  = 1;
				}
				else
				{
					$(this).addClass( 'border-success' );
					co = 0;
					errorTxt = 0;
				}
			}
		},
		keyup: function(){
			
		}
	});

	$('#apellido').on({
		focusin: function(){

			$(this).removeClass( 'border-success border-warning border-danger' );
		},
		focusout: function(){
			var elementoT = document.formulario.apellido;
			var re = /^[A-Za-zñÑÁÉÍÓÚáéíóúÿ]{2,}$/i;
			if( elementoT.value === '' )
			{
				$(this).addClass( 'border-warning' );
				co = 1;
			}
			else
			{
				if ( !re.test(formulario.apellido.value) )
				{
					$(this).addClass( 'border-danger' );
					errorTxt  = 1;
				}
				else
				{
					$(this).addClass( 'border-success' );
					co = 0;
					errorTxt = 0;
				}
			}
		},
		keyup: function(){
			
		}
	});

	$('#usuario').on({
		focusin: function(){
			$(this).removeClass( 'border-success border-warning border-danger' );
		},
		focusout: function(){
			var elemento = document.formulario.usuario;
			var re = /^[0123456789]{7,8}$/i;
			if( elemento.value === '' )
			{
				$(this).addClass( 'border-warning' );
				co = 1;
			}
			else
			{
				if ( !re.test(formulario.usuario.value) )
				{
					$(this).addClass( 'border-danger' );
					errorUsua  = 1;
				}
				else
				{
					$(this).addClass( 'border-success' );
					co = 0;
					errorUsua = 0;
				}
			}
		},
		keyup: function(){
			
		}
	});

	$('#contraseña').on({
		focusin: function(){
			$(this).removeClass( 'border-success border-warning border-danger' );
		},
		focusout: function(){
			var elemento = document.formulario.contraseña;
			var re = /^[A-Za-z0-9?/.+@#%]{8,16}$/i;
			if( elemento.value === '' )
			{
				$(this).addClass( 'border-warning' );
				co = 1;
			}
			else
			{
				if ( !re.test(formulario.contraseña.value) )
				{
					$(this).addClass( 'border-danger' );
					errorContraseña  = 1;
				}
				else
				{
					$(this).addClass( 'border-success' );
					co = 0;
					errorContraseña = 0;
				}
			}
		},
		keyup: function(){
			
		}
	});

	$('#registrar').on({
		click: function(){
			
			if ( document.formulario.nombre.value === '' )
			{
				$('#nombre').addClass( 'border-warning' );
				co = 1;
			}

			if ( document.formulario.apellido.value === '' )
			{
				$('#apellido').addClass( 'border-warning' );
				co = 1;
			}

			if ( document.formulario.usuario.value === '' )
			{
				$('#usuario').addClass( 'border-warning' );
				co = 1;
			}

			if ( document.formulario.cargo.value === '' )
			{
	        	$('#cargo').removeClass( 'border-success' );
	        	$('#cargo').addClass( 'border-warning' );
				co = 1;
			}
			else
			{
				$('#cargo').removeClass( 'border-warning' );
				$('#cargo').addClass( 'border-success' );
				co = 0;
				error = 0;
	    	}

	    	if ( document.formulario.empresa.value === '' )
			{
	        	$('#empresa').removeClass( 'border-success' );
	        	$('#empresa').addClass( 'border-warning' );
				co = 1;
			}
			else
			{
				$('#empresa').removeClass( 'border-warning' );
				$('#empresa').addClass( 'border-success' );
				co = 0;
				error = 0;
	    	}

			if ( document.formulario.contraseña.value === '' )
			{
				$('#contraseña').addClass( 'border-warning' );
				co = 1;
			}

			if ( errorTxt == 1 || errorUsua == 1 || errorContraseña == 1 || co == 1 )
			{			
				alert.set('notifier','position', 'top-right');
				alert.error("<span class='text-light'>Datos No Enviados</span>");
			}
			else
			{
				$('#enviar').html("<br><div class='container'><div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Alerta!</strong> Usuario No Registrado <button type='button' class='close' data-dismiss='alert' aria-label='Close'> <span aria-hidden='true'>&times;</span> </button> </div> </div> <br>");
				
				alert.set('notifier','position', 'top-right');
				alert.success("<h1><span class='text-light'>Usuario Registrado Exitosamente</h1></span>");
			}
		}
	});
});