<nav class="navbar navbar-expand navbar-dark sticky-top">

  <a class="navbar-brand mr-1" href="paneladmin.php" data-toggle="tooltip" title="Inicio">KTS Corp</a>

  <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
    <i class="fa fa-bars" style="font-size: 18px;"></i>
  </button>

  <!--Navbar Search-->
  <div class="d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
    <a class="nabvar-brand text-white"><?php echo $_SESSION['nombre'].' '.$_SESSION['apellido']; ?></a>
  </div>

  <!--Navbar-->
  <ul class="navbar-nav ml-auto ml-md-0">
    <li class="nav-item">
      <a class="nav-link" href="csesion.php">
        <i class="fa fa-sign-out" data-toggle="tooltip" title="Cerrar Sesión" style="font-size: 18px;"></i>
      </a>
    </li>
  </ul>
</nav>

<div id="wrapper">

  <!--Sidebar-->
  <ul class="sidebar navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="cartas.php">
        <i class="fa fa-fw fa-file-text-o"></i>
        <span>Cartas de Trabajo</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="formatos.php">
        <i class="fa fa-fw fa-file-pdf-o"></i>
        <span>Formatos</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="politicas.php">
        <i class="fa fa-fw fa-book"></i>
        <span>Políticas</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="usuarios.php">
        <i class="fa fa-fw fa-users"></i>
        <span>Mostrar Usuarios</span></a>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-fw fa-user-plus"></i>
        <span>Registrar Usuario</span>
        <i class="fa fa-caret-right"></i>
      </a>
      <div class="dropdown-menu" aria-labelledby="pagesDropdown">
        <h6 class="dropdown-header">Registrar:</h6>
        <a class="dropdown-item" href="registraradmin.php">Administrador</a>
        <a class="dropdown-item" href="registraremp.php">Empleado</a>
      </div>
    </li>
  </ul>