$(function(){

	$('#nombre').on({
		focusin: function(){
			$(this).removeClass( 'border-success border-warning border-danger' );
		},
		focusout: function(){
			var elementoT = document.contactos.nombre;
			var re = /^[A-Za-zñÑÁÉÍÓÚáéíóúÿ]{2,}$/i;
			if( elementoT.value === '' )
			{
				$(this).addClass( 'border-warning' );
				$('#enviar').removeAttr( 'type','submit' );
				$('#enviar').attr( 'type','button' );
			}
			else
			{
				if ( !re.test(contactos.nombre.value) )
				{
					$(this).addClass( 'border-danger' );
					$('#enviar').removeAttr( 'type','submit' );
					$('#enviar').attr( 'type','button' );
				}
				else
				{
					$(this).addClass( 'border-success' );
					$('#enviar').removeAttr( 'type','button' );
					$('#enviar').attr( 'type','submit' );
				}
			}
		},
		keyup: function(){
		}
	});

	$('#apellido').on({
		focusin: function(){
			$(this).removeClass( 'border-success border-warning border-danger' );
		},
		focusout: function(){
			var elemento = document.contactos.apellido;
			var re = /^[A-Za-zñÑÁÉÍÓÚáéíóúÿ]{2,}$/i;
			if( elemento.value === '' )
			{
				$(this).addClass( 'border-warning' );
				$('#enviar').removeAttr( 'type','submit' );
				$('#enviar').attr( 'type','button' );
			}
			else
			{
				if ( !re.test(contactos.apellido.value) )
				{
					$(this).addClass( 'border-danger' );
					$('#enviar').removeAttr( 'type','submit' );
					$('#enviar').attr( 'type','button' );
				}
				else
				{
					$(this).addClass( 'border-success' );
					$('#enviar').removeAttr( 'type','button' );
					$('#enviar').attr( 'type','submit' );
				}
			}
		},
		keyup: function(){
			
		}
	});

	$('#edad').on({
		focusin: function(){
			$(this).removeClass( 'border-success border-warning' );
		},
		focusout: function(){
			var elemento = document.contactos.edad;
			if( elemento.value === '' )
			{
				$(this).addClass( 'border-warning' );
				$('#enviar').removeAttr( 'type','submit' );
				$('#enviar').attr( 'type','button' );
			}
			else
			{
				$(this).addClass( 'border-success' );
				$('#enviar').removeAttr( 'type','button' );
				$('#enviar').attr( 'type','submit' );
			}
		},
		keyup: function(){
		}
	});

	$('#email').on({
		focusin: function(){
			$(this).removeClass( 'border-success border-warning border-danger' );
		},
		focusout: function(){
			var elemento = document.contactos.email;
			var re = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/i;
			if( elemento.value === '' )
			{
				$(this).addClass( 'border-warning' );
				$('#enviar').removeAttr( 'type','submit' );
				$('#enviar').attr( 'type','button' );
			}
			else
			{
				if ( !re.test(contactos.email.value) )
				{
					$(this).addClass( 'border-danger' );
					$('#enviar').removeAttr( 'type','submit' );
					$('#enviar').attr( 'type','button' );
				}
				else
				{
					$(this).addClass( 'border-success' );
					$('#enviar').removeAttr( 'type','button' );
					$('#enviar').attr( 'type','submit' );
				}
			}
		},
		keyup: function(){
			
		}
	});

	$('#celular').on({
		focusin: function(){
			$(this).removeClass( 'border-success border-warning' );
		},
		focusout: function(){
			var elemento = document.contactos.celular;
			if( elemento.value === '' )
			{
				$(this).addClass( 'border-warning' );
				$('#enviar').removeAttr( 'type','submit' );
				$('#enviar').attr( 'type','button' );
			}
			else
			{
				$(this).addClass( 'border-success' );
				$('#enviar').removeAttr( 'type','button' );
				$('#enviar').attr( 'type','submit' );
			}
		},
		keyup: function(){
		}
	});

	$('#numero').on({
		focusin: function(){
			$(this).removeClass( 'border-success border-warning border-danger' );
		},
		focusout: function(){
			var elemento = document.contactos.numero;
			var re = /^[0123456789]{7}$/i;
			if( elemento.value === '' )
			{
				$(this).addClass( 'border-warning' );
				$('#enviar').removeAttr( 'type','submit' );
				$('#enviar').attr( 'type','button' );
			}
			else
			{
				if ( !re.test(contactos.numero.value) )
				{
					$(this).addClass( 'border-danger' );
				}
				else
				{
					$(this).addClass( 'border-success' );
					$('#enviar').removeAttr( 'type','button' );
					$('#enviar').attr( 'type','submit' );
				}
			}
		},
		keyup: function(){
			
		}
	});

	$('#mensaje').on({
		focusin: function(){
			$(this).removeClass( 'border-success border-warning border-danger' );
		},
		focusout: function(){
			var elemento = document.contactos.mensaje;
			var re = /^[A-Za-z-.,0-9áéíóúÁÉÍÓÚ ]*$/i;
			if( elemento.value === '' )
			{
				$(this).addClass( 'border-warning' );
				$('#enviar').removeAttr( 'type','submit' );
				$('#enviar').attr( 'type','button' );
			}
			else
			{
				if ( !re.test(contactos.mensaje.value) )
				{
					$(this).addClass( 'border-danger' );
					$('#enviar').removeAttr( 'type','submit' );
					$('#enviar').attr( 'type','button' );
				}
				else
				{
					$(this).addClass( 'border-success' );
					$('#enviar').removeAttr( 'type','button' );
					$('#enviar').attr( 'type','submit' );
				}
			}
		},
		keyup: function(){
		}
	});

	$('#trabajo').on({
		focusin: function(){
			$(this).removeClass( 'border-success border-warning' );
		},
		focusout: function(){
			var elemento = document.contactos.trabajo;
			if( elemento.value === '' )
			{
				$(this).addClass( 'border-warning' );
				$('#enviar').removeAttr( 'type','submit' );
				$('#enviar').attr( 'type','button' );
			}
			else
			{
				$(this).addClass( 'border-success' );
				$('#enviar').removeAttr( 'type','button' );
				$('#enviar').attr( 'type','submit' );
			}
		},
		keyup: function(){
		}
	});

	$('#enviar').on({
		click: function(){

			if ( document.contactos.nombre.value === '' )
			{
				$('#nombre').addClass( 'border-warning' );
				$('#enviar').removeAttr( 'type','submit' );
				$('#enviar').attr( 'type','button' );
			}
			if ( document.contactos.apellido.value === '' )
			{
				$('#apellido').addClass( 'border-warning' );
				$('#enviar').removeAttr( 'type','submit' );
				$('#enviar').attr( 'type','button' );
			}
			if ( document.contactos.edad.value === '' )
			{
				$('#edad').addClass( 'border-warning' );
				$('#enviar').removeAttr( 'type','submit' );
				$('#enviar').attr( 'type','button' );
			}
			if ( document.contactos.email.value === '' )
			{
				$('#email').addClass( 'border-warning' );
				$('#enviar').removeAttr( 'type','submit' );
				$('#enviar').attr( 'type','button' );
			}
			if ( document.contactos.celular.value === '' )
			{
				$('#celular').addClass( 'border-warning' );
				$('#enviar').removeAttr( 'type','submit' );
				$('#enviar').attr( 'type','button' );
			}
			if ( document.contactos.numero.value === '' )
			{
				$('#numero').addClass( 'border-warning' );
				$('#enviar').removeAttr( 'type','submit' );
				$('#enviar').attr( 'type','button' );
			}
			if ( document.contactos.trabajo.value === '' )
			{
				$('#trabajo').addClass( 'border-warning' );
				$('#enviar').removeAttr( 'type','submit' );
				$('#enviar').attr( 'type','button' );
			}
			if ( document.contactos.mensaje.value === '' )
			{
				$('#mensaje').addClass( 'border-warning' );
				$('#enviar').removeAttr( 'type','submit' );
				$('#enviar').attr( 'type','button' );
			}
		}
	});
});