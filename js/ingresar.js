$(function(){

	$('#cedula').on({
		focusin: function(){
			$(this).removeClass( 'border-success border-warning border-danger' );
		},
		focusout: function(){
			var elemento = document.ingresar.cedula;
			var re = /^[0-9]{7,8}$/i;
			if( elemento.value === '' )
			{
				$(this).addClass( 'border-warning' );
				$('#continuar').removeAttr( 'type','submit' );
				$('#continuar').attr( 'type','button' );
			}
			else
			{
				if ( !re.test(ingresar.cedula.value) )
				{
					$(this).addClass( 'border-danger' );
					$('#continuar').removeAttr( 'type','submit' );
					$('#continuar').attr( 'type','button' );
				}
				else
				{
					$(this).addClass( 'border-success' );
					$('#continuar').removeAttr( 'type','button' );
					$('#continuar').attr( 'type','submit' );
				}
			}
		},
		keyup: function(){
		}
	});

	$('#contraseña').on({
		focusin: function(){
			$(this).removeClass( 'border-success border-warning border-danger' );
		},
		focusout: function(){
			var elementoT = document.ingresar.contraseña;
			if( elementoT.value === '' )
			{
				$(this).addClass( 'border-warning' );
				$('#continuar').removeAttr( 'type','submit' );
				$('#continuar').attr( 'type','button' );
			}
			else
			{
				$(this).addClass( 'border-success' );
				$('#continuar').removeAttr( 'type','button' );
				$('#continuar').attr( 'type','submit' );
			}
		},
		keyup: function(){
		}
	});

	$('#opctipo').on({
		focusin: function(){
			$(this).removeClass( 'border-success border-warning border-danger' );
		},
		focusout: function(){
			var elementoT = document.ingresar.opctipo;
			if( elementoT.value === '' )
			{
				$(this).addClass( 'border-warning' );
				$('#continuar').removeAttr( 'type','submit' );
				$('#continuar').attr( 'type','button' );
			}
			else
			{
				$(this).addClass( 'border-success' );
				$('#continuar').removeAttr( 'type','button' );
				$('#continuar').attr( 'type','submit' );
			}
		},
		keyup: function(){
		}
	});

	$('#continuar').on({
		click: function(){

			if ( document.ingresar.cedula.value === '' )
			{
				$('#cedula').addClass( 'border-warning' );
				$('#continuar').removeAttr( 'type','submit' );
				$('#continuar').attr( 'type','button' );
			}

			if ( document.ingresar.contraseña.value === '' )
			{
				$('#contraseña').addClass( 'border-warning' );
				$('#continuar').removeAttr( 'type','submit' );
				$('#continuar').attr( 'type','button' );
			}

			if ( document.ingresar.opctipo.value === '' )
			{
				$('#opctipo').addClass( 'border-warning' );
				$('#continuar').removeAttr( 'type','submit' );
				$('#continuar').attr( 'type','button' );
			}
		}
	});
});