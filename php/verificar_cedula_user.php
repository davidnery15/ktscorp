<?php
    include_once('conexion/cnx.php');  //Agregando la Conexión a Base de Datos

    //Obtenemos el parámetro el URL
    $ci = $_REQUEST["ver_cedula"];

    //Estableciendo el arreglo de las cedulas en la base de datos
    $Q = "SELECT cedula FROM usuarios";
    $QBC = mysqli_query($cnx, $Q) or die(mysqli_error($cnx));

    //Declarando la variable para mostrar la cedula registrada
    $mostrar_ci = "";

    //Busca las coincidencias desde el arreglo, devuelve valor si prenda es diferente a ""
    while ( $a=mysqli_fetch_array($QBC) ) {
        if ($ci !== "") {
            $len=strlen($ci);
            foreach($a as $nombre) {
                if (stristr($ci, substr($nombre, 0, $len))) {
                    $mostrar_ci = $nombre;
                }
            }
        }
    }

    //Si $mostrar_ci tiene alguna coincidencia se muestra en pantalla"
    if($mostrar_ci !== ""){
        echo 'La cedula ingresada ya está registrada: '.$mostrar_ci;
    }
?>