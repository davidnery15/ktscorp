$(function(){
	
	$('#nombre').on({
		focusin: function(){
			$(this).removeClass( 'border-success border-warning border-danger' );
		},
		focusout: function(){
			var elementoT = document.registro.nombre;
			var re = /^[A-Za-zñÑÁÉÍÓÚáéíóúÿ]{2,}$/i;
			if( elementoT.value === '' )
			{
				$(this).addClass( 'border-warning' );
				$('#registrar').removeAttr( 'type','submit' );
				$('#registrar').attr( 'type','button' );
			}
			else
			{
				if ( !re.test(registro.nombre.value) )
				{
					$(this).addClass( 'border-danger' );
					$('#registrar').removeAttr( 'type','submit' );
					$('#registrar').attr( 'type','button' );
				}
				else
				{
					$(this).addClass( 'border-success' );
					$('#registrar').removeAttr( 'type','button' );
					$('#registrar').attr( 'type','submit' );
				}
			}
		},
		keyup: function(){
		}
	});

	$('#apellido').on({
		focusin: function(){
			$(this).removeClass( 'border-success border-warning border-danger' );
		},
		focusout: function(){
			var elementoT = document.registro.apellido;
			var re = /^[A-Za-zñÑÁÉÍÓÚáéíóúÿ]{2,}$/i;
			if( elementoT.value === '' )
			{
				$(this).addClass( 'border-warning' );
				$('#registrar').removeAttr( 'type','submit' );
				$('#registrar').attr( 'type','button' );
			}
			else
			{
				if ( !re.test(registro.apellido.value) )
				{
					$(this).addClass( 'border-danger' );
					$('#registrar').removeAttr( 'type','submit' );
					$('#registrar').attr( 'type','button' );
				}
				else
				{
					$(this).addClass( 'border-success' );
					$('#registrar').removeAttr( 'type','button' );
					$('#registrar').attr( 'type','submit' );
				}
			}
		},
		keyup: function(){
		}
	});

	$('#cedula').on({
		focusin: function(){
			$(this).removeClass( 'border-success border-warning border-danger' );
		},
		focusout: function(){
			var elemento = document.registro.cedula;
			var re = /^[0-9]{7,8}$/i;
			if( elemento.value === '' )
			{
				$(this).addClass( 'border-warning' );
				$('#registrar').removeAttr( 'type','submit' );
				$('#registrar').attr( 'type','button' );
			}
			else
			{
				if ( !re.test(registro.cedula.value) )
				{
					$(this).addClass( 'border-danger' );
					$('#registrar').removeAttr( 'type','submit' );
					$('#registrar').attr( 'type','button' );
				}
				else
				{
					$(this).addClass( 'border-success' );
					$('#registrar').removeAttr( 'type','button' );
					$('#registrar').attr( 'type','submit' );
				}
			}
		},
		keyup: function(){
		}
	});

	$('#clave').on({
		focusin: function(){
			$(this).removeClass( 'border-success border-warning border-danger' );
		},
		focusout: function(){
			var elemento = document.registro.clave;
			var re = /^[A-Za-z0-9?/.+@#%]{8,16}$/i;
			if( elemento.value === '' )
			{
				$(this).addClass( 'border-warning' );
				$('#registrar').removeAttr( 'type','submit' );
				$('#registrar').attr( 'type','button' );
			}
			else
			{
				if ( !re.test(registro.clave.value) )
				{
					$(this).addClass( 'border-danger' );
					$('#registrar').removeAttr( 'type','submit' );
					$('#registrar').attr( 'type','button' );
				}
				else
				{
					$(this).addClass( 'border-success' );
					$('#registrar').removeAttr( 'type','button' );
					$('#registrar').attr( 'type','submit' );
				}
			}
		},
		keyup: function(){
		}
	});

	$('#cargo').on({
		focusin: function(){
			$(this).removeClass( 'border-success border-warning border-danger' );
		},
		focusout: function(){
			var elemento = document.registro.cargo;
			if( elemento.value === '' )
			{
				$(this).addClass( 'border-warning' );
				$('#registrar').removeAttr( 'type','submit' );
				$('#registrar').attr( 'type','button' );
			}
			else
			{
				$(this).addClass( 'border-success' );
				$('#registrar').removeAttr( 'type','button' );
				$('#registrar').attr( 'type','submit' );
			}
		},
		keyup: function(){
		}
	});

	$('#empresa').on({
		focusin: function(){
			$(this).removeClass( 'border-success border-warning border-danger' );
		},
		focusout: function(){
			var elemento = document.registro.empresa;
			if( elemento.value === '' )
			{
				$(this).addClass( 'border-warning' );
				$('#registrar').removeAttr( 'type','submit' );
				$('#registrar').attr( 'type','button' );
			}
			else
			{
				$(this).addClass( 'border-success' );
				$('#registrar').removeAttr( 'type','button' );
				$('#registrar').attr( 'type','submit' );
			}
		},
		keyup: function(){
		}
	});
	
	$('#registrar').on({
		click: function(){

			if ( document.registro.nombre.value === '' )
			{
				$('#nombre').addClass( 'border-warning' );
				$('#registrar').removeAttr( 'type','submit' );
				$('#registrar').attr( 'type','button' );
			}

			if ( document.registro.apellido.value === '' )
			{
				$('#apellido').addClass( 'border-warning' );
				$('#registrar').removeAttr( 'type','submit' );
				$('#registrar').attr( 'type','button' );
			}

			if ( document.registro.cedula.value === '' )
			{
				$('#cedula').addClass( 'border-warning' );
				$('#registrar').removeAttr( 'type','submit' );
				$('#registrar').attr( 'type','button' );
			}

			if ( document.registro.clave.value === '' )
			{
				$('#clave').addClass( 'border-warning' );
				$('#registrar').removeAttr( 'type','submit' );
				$('#registrar').attr( 'type','button' );
			}
			
			if ( document.registro.cargo.value === '' )
			{
				$('#cargo').addClass( 'border-warning' );
				$('#registrar').removeAttr( 'type','submit' );
				$('#registrar').attr( 'type','button' );
			}
			
			if ( document.registro.empresa.value === '' )
			{
				$('#empresa').addClass( 'border-warning' );
				$('#registrar').removeAttr( 'type','submit' );
				$('#registrar').attr( 'type','button' );
			}
		}
	});
});