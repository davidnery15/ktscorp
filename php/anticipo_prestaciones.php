<?php
    include_once("sesion.php");
    include_once("conexion/cnx.php");
    include_once('../fpdf/fpdf.php');


        class PDF extends FPDF {
            function Header() {
                $this->SetFont('Arial','B',15);
                $this->Cell(12);
            }
            function Footer() {

                $this->Cell(180,0,'','T',1,'',true);

                $this->SetY(-15);
                
                $this->SetFont('Arial','',8);
            }
        }
        
        $pdf = new PDF('P','mm','A4');

        $pdf->AliasNbPages('1');
        
        $pdf->SetAutoPageBreak(true,15);
        $pdf->AddPage();
        
        $pdf->SetFont('Arial','',9);
        $pdf->SetDrawColor(180,180,255);
        $cont = 1;
        
        $pdf = new PDF();
        $pdf->AddPage();

        $pdf->SetFont('Arial','B',10);
        $pdf->SetFillColor(255,255,255);
        $pdf->SetDrawColor(255,255,255);
        $pdf->Ln(50);
        $pdf->SetDrawColor(180,180,255);
        $pdf->Cell(187,12,'DEPARTAMENTO DE TALENTO HUMANO',1,1,'C', true);
        $pdf->Cell(93.5,10,'SOLICITUD DE ANTICIPO PRESTACIONES SOCILAES',1,0,'C', true);
        $pdf->Cell(93.5,10,utf8_decode('CÓDIGO: TH-F07'),1,0,'C', true);
        $pdf->Ln(15);
        $pdf->Cell(187.5,5,utf8_decode('Maracaibo,____ de ______ del año ____.'),0,0,'R',true);
        $pdf->Ln(10);
        $pdf->Cell(20,5,utf8_decode('Señores:'),0,1,'',true);
        $pdf->Ln(4);
        $pdf->Cell(22,5,'KTS Corp.',0,0,'',true);
        $pdf->Cell(5,4,'',1,0,'',true);
        $pdf->Cell(22,5,'Max Ferrer',0,0,'C',true);
        $pdf->Cell(5,4,'',1,0,'',true);
        $pdf->Cell(16,5,'Anatel',0,0,'',true);
        $pdf->Cell(5,4,'',1,0,'',true);
        $pdf->Cell(22,5,'Mindforce',0,0,'',true);
        $pdf->Cell(5,4,'',1,0,'',true);
        $pdf->Cell(18,5,'Neutron',0,0,'',true);
        $pdf->Cell(5,4,'',1,0,'',true);
        $pdf->Cell(18,5,'Netdata',0,0,'',true);
        $pdf->Cell(5,4,'',1,0,'',true);
        $pdf->Ln(10);
        $pdf->Cell(185,5,'Por medio de la presente, Yo ___________________ titular de la C.I. No.________________, me dirijo a ustedes',0,0,'C',true);
        $pdf->Ln(5);
        $pdf->Cell(166,5,utf8_decode('para solicitar el anticipo sobre el 75% de lo depositado como garantía de las prestaciones sociales'),0,0,'C',true);
        $pdf->Ln(5);
        $pdf->Cell(176,5,'acumuladas en la contabilidad de la empresa hasta la fecha, de acuerdo con lo establecido en el Art. 144',0,0,'C',true);
        $pdf->Ln(5);
        $pdf->Cell(160,5,utf8_decode('de la LOTTT. El monto solicitado es de ____________________ BOLÍVARES CON ________/100).'),0,0,'C',true);
        $pdf->Ln(5);
        $pdf->Cell(44,5,'(__________________ BS.)',0,0,'C',true);
        $pdf->Ln(5);
        $pdf->Cell(62,5,'El motivo de mi solicitud obedece a:',0,1,'C',true);
        $pdf->Ln(10);
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(150,10,utf8_decode('a) Construcción, adquisición, mejora o reparación de vivienda para él y su familia.'),1,0,'L',true);
        $pdf->Cell(37,10,'',1,1,'C',true);
        $pdf->Cell(150,10,utf8_decode('b) Liberación de hipoteca o cualquier otra vivienda de su propiedad.'),1,0,'L',true);
        $pdf->Cell(37,10,'',1,1,'C',true);
        $pdf->Cell(150,10,utf8_decode('c) Inversión en educación para él, su cónyuge, hijos o con quien haga vida marital.'),1,0,'L',true);
        $pdf->Cell(37,10,'',1,1,'C',true);
        $pdf->Cell(150,10,utf8_decode('d) Los gastos de atención médica y hospitalaria de las personas indicadas en el literal anterior.'),1,0,'L',true);
        $pdf->Cell(37,10,'',1,1,'C',true);
        $pdf->Ln(5);
        $pdf->Cell(187,5,'Sin otro particular, me suscribo de ustedes',0,0,'C',true);
        $pdf->Ln(10);
        $pdf->Cell(187,5,'Atentamente:',0,0,'C',true);
        $pdf->Ln(10);
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(67,25,'Nombre del Solicitante:',1,0,'L',true);
        $pdf->Cell(50,35,'',1,0,'C',true);
        $pdf->Cell(70,25,'Solicitud Autorizada por el Jefe Inmediato:',1,1,'L',true);
        $pdf->Ln(0);
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(67,25,'Firma del Solicitante:',1,0,'L',true);
        $pdf->Cell(50,25,'Huella Dactilar del Trabajador',1,0,'C',true);
        $pdf->Cell(70,25,'Recibido por: Dpto. Talento Humano:',1,1,'L',true);
        
        ob_end_clean();
        $pdf->OutPut();
?>