<?php
    include_once("sesion.php");
    include_once("conexion/cnx.php");
    include_once('../fpdf/fpdf.php');

        class PDF extends FPDF {
            function Header() {
                $this->SetFont('Arial','B',15);
                $this->Cell(12);
            }
            function Footer() {
                $this->Cell(180,0,'','T',1,'',true);
                
                $this->SetY(-15);
                
                $this->SetFont('Arial','',8);
            }
        }
        
        $pdf = new PDF('P','mm','A4');

        $pdf->AliasNbPages('1');
        
        $pdf->SetAutoPageBreak(true,15);
        $pdf->AddPage();
        
        $pdf->SetFont('Arial','',9);
        $pdf->SetDrawColor(180,180,255);
        $cont = 1;
        
        $pdf = new PDF();
        $pdf->AddPage();
        
        $pdf->SetFont('Arial','B',10);
        $pdf->SetFillColor(255,255,255);
        $pdf->SetDrawColor(255,255,255);
        $pdf->Ln(50);
        $pdf->SetDrawColor(180,180,255);
        $pdf->Cell(187,12,'DEPARTAMENTO DE TALENTO HUMANO',1,1,'C', true);
        $pdf->Cell(93.5,10,'SOLICITUD DE VACACIONES',1,0,'C', true);
        $pdf->Cell(93.5,10,utf8_decode('CÓDIGO: TH-F02'),1,0,'C', true);
        $pdf->Ln(15);
        $pdf->Cell(187.5,5,utf8_decode('Maracaibo,____ de ______ del año ____.'),0,0,'R',true);
        $pdf->Ln(10);
        $pdf->Cell(20,5,utf8_decode('Señores:'),0,1,'',true);
        $pdf->Ln(4);
        $pdf->Cell(22,5,'KTS Corp.',0,0,'',true);
        $pdf->Cell(5,4,'',1,0,'',true);
        $pdf->Cell(22,5,'Max Ferrer',0,0,'C',true);
        $pdf->Cell(5,4,'',1,0,'',true);
        $pdf->Cell(16,5,'Anatel',0,0,'',true);
        $pdf->Cell(5,4,'',1,0,'',true);
        $pdf->Cell(22,5,'Mindforce',0,0,'',true);
        $pdf->Cell(5,4,'',1,0,'',true);
        $pdf->Cell(18,5,'Neutron',0,0,'',true);
        $pdf->Cell(5,4,'',1,0,'',true);
        $pdf->Cell(18,5,'Netdata',0,0,'',true);
        $pdf->Cell(5,4,'',1,0,'',true);
        $pdf->Ln(10);
        $pdf->Cell(187,5,'Por medio de la presente, Yo ___________________ titular de la C.I. No.________________, me dirijo a ustedes',0,0,'C',true);
        $pdf->Ln(5);
        $pdf->Cell(187,5,'para solicitar el disfrute de mis vacaciones correspondientes al periodo ________________-________________,',0,0,'C',true);
        $pdf->Ln(5);
        $pdf->Cell(156,5,utf8_decode('a partir del día: ________/_________/________ hasta el día: ________/_________/_________.'),0,0,'C',true);
        $pdf->Ln(15);
        $pdf->Cell(187,5,'Sin otro particular, me suscribo de ustedes',0,0,'C',true);
        $pdf->Ln(10);
        $pdf->Cell(187,5,'Atentamente:',0,0,'C',true);
        $pdf->Ln(10);
        $pdf->Cell(67,17,'',1,0,'C',true);
        $pdf->Cell(50,17,'',1,0,'C',true);
        $pdf->Cell(70,17,'',1,0,'C',true);
        $pdf->Ln(10);
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(67,10,'Firma del Solicitante',1,0,'C',true);
        $pdf->Cell(50,10,'Huella Dactilar del Trabajador',1,0,'C',true);
        $pdf->Cell(70,10,'Solicitud Autorizada por el Jefe Inmediato',1,1,'C',true);
        $pdf->Ln(5);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(40,10,'OBSERVACIONES:',0,1,'C',true);
        $pdf->Cell(187,10,'_____________________________________________________________________________________________',0,1,'',true);
        $pdf->Cell(187,10,'_____________________________________________________________________________________________',0,1,'',true);
        $pdf->Cell(187,10,'_____________________________________________________________________________________________',0,1,'',true);
        $pdf->Cell(187,10,'_____________________________________________________________________________________________',0,1,'',true);
        $pdf->Ln(6);
        $pdf->Cell(93.5,5,'Revisado por:',1,0,'C',true);
        $pdf->Cell(93.5,5,'Aprobado por:',1,1,'C',true);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(93.5,20,'Analista de Talento Humano',1,0,'C',true);
        $pdf->Cell(93.5,20,'Coordinador de Talento Humano',1,1,'C',true);

        ob_end_clean();
        $pdf->OutPut();
?>