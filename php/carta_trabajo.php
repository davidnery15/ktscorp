<?php
    include_once("sesion.php");
    include_once("conexion/cnx.php");

    if ( isset($_POST['solicitar']) ){
        // Captando variables
        $dirigida = $_POST['dirigida'];
        $fecha    = $_POST['fecha'];
        $fecha    = date("d-m-Y",strtotime($fecha));

        // Captando variables globales
        $nombre   = $_SESSION['nombre'];
        $apellido = $_SESSION['apellido'];
        $cedula   = $_SESSION['cedula'];
        $opc_user = $_SESSION['opc_user'];

        // Consultando el valor del cargo
        $Q = " SELECT idcargo FROM $opc_user WHERE cedula = '$cedula' ";
        $QB = mysqli_query($cnx, $Q) or die(mysqli_error($cnx));
        while ( $fila=mysqli_fetch_array($QB) )
		{
            $idcargo = $fila['idcargo'];
        }

        $C = " SELECT descripcargo FROM usuarios_cargo WHERE idcargo = '$idcargo' ";
        $CB = mysqli_query($cnx, $C) or die(mysqli_error($cnx));
        while ( $fila=mysqli_fetch_array($CB) )
		{
            $descripcargo = $fila['descripcargo'];
        }

        //Captando el valor de la empresa
        $Q1 = " SELECT idempresa FROM $opc_user WHERE cedula = '$cedula' ";
        $QB1 = mysqli_query($cnx, $Q1) or die(mysqli_error($cnx));
        while ( $fila=mysqli_fetch_array($QB1) )
		{
            $idempresa = $fila['idempresa'];
        }

        $C1 = " SELECT descripempresa FROM usuarios_empresa WHERE idempresa = '$idempresa' ";
        $CB1 = mysqli_query($cnx, $C1) or die(mysqli_error($cnx));
        while ( $fila=mysqli_fetch_array($CB1) )
		{
            $descripempresa = $fila['descripempresa'];
        }

        // Preparando el correo

        $to = 'davidnery15@gmail.com';
        $subject = 'Peticion de Carta de Trabajo';
        $from = 'x@gmail.com';
        $fromName = $nombre." ".$apellido;
        $headers = "From: $fromName"." <".$from.">";

        $message  = "Nombre: ".$nombre." ".$apellido;
        $message .= "\nCedula: ".$cedula;
        $message .= "\nEmpresa: ".$descripempresa;
        $message .= "\nCargo: ".$descripcargo;
        $message .= "\nDirigida a: ".$dirigida;
        $message .= "\nFecha: ".$fecha;

        $mail = @mail($to, $subject, $message, $headers); 

        //Mensaje verificando que se enviaron los datos correctamente
        if($mail){
            echo '<script> alert("Datos enviados"); location.href="cartas.php"; </script>';
        }else{
            echo '<script> alert("Error al enviar los datos"); location.href="cartas.php"; </script>';
        }
    }
?>